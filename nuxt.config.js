const postcssCustomMedia = require("postcss-custom-media");
const postcssImport = require("postcss-import");
const postcssPresetEnv = require("postcss-preset-env");
const postcssReporter = require("postcss-reporter");
const postcssNested = require("postcss-nested");
const postcssCalc = require("postcss-calc");
const StyleLintPlugin = require("stylelint-webpack-plugin");

require("dotenv").config();

module.exports = {
  env: {
    baseUrl: process.env.BASE_URL,
    apiUrl: process.env.API_URL,
    NODE_ENV: process.env.NODE_ENV || "development",
  },
  head: {
    htmlAttrs: {
      lang: "nb",
    },
    title: "Vinduet.no",
    titleTemplate: (titleChunk) => (titleChunk ? `${titleChunk} | Vinduet` : "Vinduet"),
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Vinduet" },
    ],
    link: [{ rel: "icon", type: "image /x-icon", href: "/favicon.ico" }],
    script: [
      {
        type: "module",
        src: "/js/app.jsvh.js",
        body: true,
      },
    ],
  },
  loading: { color: "#00543B" },
  buildModules: ["@nuxtjs/eslint-module", "@nuxt/typescript-build", "@nuxtjs/svg"],
  css: [{ src: "~assets/css/master.css" }],
  build: {
    extractCSS: false,
    optimizeCSS: true,
    postcss: {
      plugins: [
        postcssImport(),
        postcssPresetEnv({
          stage: 2, // default stage is 2
          preserve: false,
          importFrom: ["assets/css/_grid.css", "assets/css/_variables.css"],
          autoprefixer: {
            grid: true,
          },
          features: {
            "color-mod-function": { unresolved: "warn" },
            "custom-media-queries": {},
            "double-position-gradients": false,
          },
          browsers: [">= 5% in DK", "ie 11"],
        }),
        postcssCustomMedia({
          importFrom: ["assets/css/_mediaqueries.css"],
        }),
        postcssNested(),
        postcssCalc(),
        postcssReporter({
          clearReportedMessages: true,
        }),
      ],
    },
    babel: {
      plugins: [
        ["@babel/plugin-proposal-decorators", { legacy: true }],
        ["@babel/plugin-proposal-class-properties", { loose: true }],
      ],
    },
    /*extend(config, { isDev }) { // POSTCSS NOT SUPPORTED
      // Stylelint
      config.plugins.push(
        new StyleLintPlugin({
          syntax: 'scss' // eg. with options if you need SCSS ;-)
        })
      )
    },*/
  },
  modules: ["@nuxtjs/axios", "@nuxtjs/svg-sprite"],
  svgSprite: {
    input: "~/assets/svg",
  },
  axios: {
    baseURL: process.env.API_URL,
  },
  plugins: ["~/plugins/i18n", "~/plugins/apis", "~/plugins/lazyload.client", "~/plugins/filters"],
  render: {
    injectScripts: false,
    resourceHints: false,
    http2: {
      push: true,
      pushAssets: (req, res, publicPath, preloadFiles) => (preloadFiles = []),
    },
    static: {
      maxAge: "1y",
      setHeaders(res, path) {
        if (path.includes("sw.js")) {
          res.setHeader("Cache-Control", `public, max-age=${15 * 60}`);
        }
      },
    },
  },
  server: {
    port: process.env.NODE_PORT || 3200,
    host: process.env.HOST,
    timing: {
      total: true,
    },
  },
  router: {
    prefetchLinks: false,
    extendRoutes(routes, resolve) {
      routes.splice(0, routes.length);
      routes.push({
        name: "content",
        path: "*",
        component: resolve(__dirname, "pages/index.vue"),
      });
    },
  },
};
