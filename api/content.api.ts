export default ($axios: any) => ({
  getContentByUrl(path: string): any {
    return $axios("/api/content", {
      method: "get",
      params: {
        url: path,
      },
    });
  },

  getSettings(path: string): any {
    return $axios("/api/settings?url=https://vinduetstaging.azurewebsites.net&site=Vinduet", {
      method: "get",
      params: {
        url: path,
      },
    });
  },
});
