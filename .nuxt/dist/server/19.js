exports.ids = [19];
exports.modules = {

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./pages/index.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};


const templates = {
  Effrontpage: () => __webpack_require__.e(/* import() */ 17).then(__webpack_require__.bind(null, 155)),
  EftextPage: () => __webpack_require__.e(/* import() */ 18).then(__webpack_require__.bind(null, 156)),
  EfarchivePage: () => __webpack_require__.e(/* import() */ 15).then(__webpack_require__.bind(null, 157)),
  EfauthorsPage: () => __webpack_require__.e(/* import() */ 16).then(__webpack_require__.bind(null, 158)),
  EfcategoryPage: () => __webpack_require__.e(/* import() */ 11).then(__webpack_require__.bind(null, 159)),
  EfgenrePage: () => __webpack_require__.e(/* import() */ 12).then(__webpack_require__.bind(null, 160)),
  Efarticle: () => __webpack_require__.e(/* import() */ 4).then(__webpack_require__.bind(null, 152)),
  Ef404Page: () => __webpack_require__.e(/* import() */ 10).then(__webpack_require__.bind(null, 161)),
  Ef500Page: () => __webpack_require__.e(/* import() */ 14).then(__webpack_require__.bind(null, 162))
};
let lib_vue_loader_options_pagesvue_type_script_lang_ts_IndexPage = class IndexPage extends nuxt_property_decorator_common["Vue"] {
  asyncData(ctx) {
    return ctx.payload;
  }

  render(h) {
    if (!this.meta.template) {
      console.error(`No render for ${this.meta.template}`); // eslint-disable-line no-console

      return h();
    }

    const template = `Ef${this.meta.template}`;

    if (!templates[template]) {
      console.error(`No render for ${template}`); // eslint-disable-line no-console

      return h();
    }

    const page = h(template, {
      props: {
        content: this.content
      }
    });
    return h("main", {}, [page]);
  }

  head() {
    if (!this.seo) {
      return {};
    }

    let robots = this.seo.index ? this.seo.index : "";

    if (robots && robots !== "") {
      robots += ",";
    }

    robots += this.seo.follow ? this.seo.follow : "";
    return {
      title: this.seo.title || "",
      link: [{
        rel: "canonical",
        href: this.seo.canonical
      }],
      htmlAttrs: {
        lang: this.meta.culture && this.meta.culture.lang ? this.meta.culture.lang : "en",
        amp: true
      },
      meta: [{
        hid: "description",
        name: "description",
        content: this.seo.description || ""
      }, {
        hid: "keywords",
        name: "keywords",
        content: this.seo.keywords || ""
      }, {
        hid: "robots",
        name: "robots",
        content: robots
      }, {
        hid: "og:title",
        name: "og:title",
        content: this.seo.title || ""
      }]
    };
  }

  mounted() {
    if (this.meta) {
      this.$store.dispatch("navigation/SET_PATH", {
        path: this.meta.path
      });
    }

    if (false) {}
  }

};
lib_vue_loader_options_pagesvue_type_script_lang_ts_IndexPage = __decorate([Object(nuxt_property_decorator_common["Component"])({
  components: { ...templates
  },
  middleware: ["content"]
})], lib_vue_loader_options_pagesvue_type_script_lang_ts_IndexPage);
/* harmony default export */ var lib_vue_loader_options_pagesvue_type_script_lang_ts_ = (lib_vue_loader_options_pagesvue_type_script_lang_ts_IndexPage);
// CONCATENATED MODULE: ./pages/index.vue?vue&type=script&lang=ts&
 /* harmony default export */ var pagesvue_type_script_lang_ts_ = (lib_vue_loader_options_pagesvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./pages/index.vue
var render, staticRenderFns




/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pagesvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "78102478"
  
)

/* harmony default export */ var pages = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=19.js.map