exports.ids = [7];
exports.modules = {

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.runtime.common.js
var vue_runtime_common = __webpack_require__(0);
var vue_runtime_common_default = /*#__PURE__*/__webpack_require__.n(vue_runtime_common);

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./mixins/GridEditorMixin.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



class GridEditorMixinvue_type_script_lang_ts_default_1 extends vue_runtime_common_default.a {}

__decorate([Object(nuxt_property_decorator_common["Prop"])({
  default: {}
})], GridEditorMixinvue_type_script_lang_ts_default_1.prototype, "viewModel", void 0);

__decorate([Object(nuxt_property_decorator_common["Prop"])({
  default: 12
})], GridEditorMixinvue_type_script_lang_ts_default_1.prototype, "size", void 0);
// CONCATENATED MODULE: ./mixins/GridEditorMixin.vue?vue&type=script&lang=ts&
 /* harmony default export */ var GridEditorMixinvue_type_script_lang_ts_ = (GridEditorMixinvue_type_script_lang_ts_default_1); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./mixins/GridEditorMixin.vue
var render, staticRenderFns




/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  GridEditorMixinvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "cb88482c"
  
)

/* harmony default export */ var GridEditorMixin = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 116:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(131);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(7).default
module.exports.__inject__ = function (context) {
  add("7d67dc09", content, false, context)
};

/***/ }),

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ImageComponent_vue_vue_type_style_index_0_id_2094a2d6_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(116);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ImageComponent_vue_vue_type_style_index_0_id_2094a2d6_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ImageComponent_vue_vue_type_style_index_0_id_2094a2d6_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ImageComponent_vue_vue_type_style_index_0_id_2094a2d6_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ImageComponent_vue_vue_type_style_index_0_id_2094a2d6_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 131:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".image-component[data-v-2094a2d6]{justify-content:center;align-items:center;margin-bottom:0}@media (min-width:992px){.is-bigger[data-v-2094a2d6]{min-width:796px;position:relative;transform:translateX(-50%);left:50%;z-index:-1}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 164:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/grid/editors/ImageComponent.vue?vue&type=template&id=2094a2d6&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"image-component row"},[_vm._ssrNode(((_vm.viewModel.image)?("<div class=\"image-component__image col-xs-12\" data-v-2094a2d6><figure"+(_vm._ssrClass(null,(_vm.viewModel.image.isBigger)?'is-bigger':''))+" data-v-2094a2d6><img"+(_vm._ssrAttr("src",_vm.viewModel.image.url))+(_vm._ssrAttr("alt",_vm.viewModel.image.alt))+" data-v-2094a2d6> "+((_vm.viewModel.image.title)?("<figcaption class=\"center-xs\" data-v-2094a2d6>"+_vm._ssrEscape("\n        "+_vm._s(_vm.viewModel.image.title)+"\n      ")+"</figcaption>"):"<!---->")+"</figure></div>"):"<!---->"))])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/grid/editors/ImageComponent.vue?vue&type=template&id=2094a2d6&scoped=true&

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./mixins/GridEditorMixin.vue + 2 modules
var GridEditorMixin = __webpack_require__(110);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./components/grid/editors/ImageComponent.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let ImageComponentvue_type_script_lang_ts_ImageComponent = class ImageComponent extends GridEditorMixin["a" /* default */] {};
ImageComponentvue_type_script_lang_ts_ImageComponent = __decorate([nuxt_property_decorator_common["Component"]], ImageComponentvue_type_script_lang_ts_ImageComponent);
/* harmony default export */ var ImageComponentvue_type_script_lang_ts_ = (ImageComponentvue_type_script_lang_ts_ImageComponent);
// CONCATENATED MODULE: ./components/grid/editors/ImageComponent.vue?vue&type=script&lang=ts&
 /* harmony default export */ var editors_ImageComponentvue_type_script_lang_ts_ = (ImageComponentvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/grid/editors/ImageComponent.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(130)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  editors_ImageComponentvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "2094a2d6",
  "eac60924"
  
)

/* harmony default export */ var editors_ImageComponent = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=7.js.map