exports.ids = [3];
exports.modules = {

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.runtime.common.js
var vue_runtime_common = __webpack_require__(0);
var vue_runtime_common_default = /*#__PURE__*/__webpack_require__.n(vue_runtime_common);

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./mixins/GridEditorMixin.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



class GridEditorMixinvue_type_script_lang_ts_default_1 extends vue_runtime_common_default.a {}

__decorate([Object(nuxt_property_decorator_common["Prop"])({
  default: {}
})], GridEditorMixinvue_type_script_lang_ts_default_1.prototype, "viewModel", void 0);

__decorate([Object(nuxt_property_decorator_common["Prop"])({
  default: 12
})], GridEditorMixinvue_type_script_lang_ts_default_1.prototype, "size", void 0);
// CONCATENATED MODULE: ./mixins/GridEditorMixin.vue?vue&type=script&lang=ts&
 /* harmony default export */ var GridEditorMixinvue_type_script_lang_ts_ = (GridEditorMixinvue_type_script_lang_ts_default_1); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./mixins/GridEditorMixin.vue
var render, staticRenderFns




/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  GridEditorMixinvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "cb88482c"
  
)

/* harmony default export */ var GridEditorMixin = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 120:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(139);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(7).default
module.exports.__inject__ = function (context) {
  add("f722c7f4", content, false, context)
};

/***/ }),

/***/ 121:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(141);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(7).default
module.exports.__inject__ = function (context) {
  add("526db81c", content, false, context)
};

/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_GylImage_vue_vue_type_style_index_0_id_7279fd9a_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(120);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_GylImage_vue_vue_type_style_index_0_id_7279fd9a_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_GylImage_vue_vue_type_style_index_0_id_7279fd9a_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_GylImage_vue_vue_type_style_index_0_id_7279fd9a_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_GylImage_vue_vue_type_style_index_0_id_7279fd9a_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 139:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "img[data-v-7279fd9a]{width:100%;height:auto;max-height:100%;flex-shink:0;transition:opacity .2s ease-in;display:block}img.lazyload[data-v-7279fd9a]:not([src]){opacity:0;visibility:hidden}img.lazyloaded[data-v-7279fd9a]{opacity:1}img.lazyload-bg[data-v-7279fd9a]{opacity:0;visibility:hidden;width:0;height:0}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleListVinduetRow_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(121);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleListVinduetRow_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleListVinduetRow_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleListVinduetRow_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleListVinduetRow_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 141:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".dynamic-articles-row-component{padding:0;margin:0 0 20px}.dynamic-articles-row-component__header{text-align:center;margin-bottom:15px;text-transform:uppercase}.dynamic-articles-row-component .article-col a{color:inherit;text-decoration:none}.dynamic-articles-row-component .article-col__inner{height:100%;margin:0}.dynamic-articles-row-component .article-col__inner-text-wrapper{height:100%;max-width:25%;flex-basis:25%;padding-right:30px}.dynamic-articles-row-component .article-col__inner-text-wrapper-introduction{font-size:14px;margin-bottom:15px}.dynamic-articles-row-component .article-col__inner-text-wrapper-title{font-size:30px;line-height:35px;margin-bottom:15px}.dynamic-articles-row-component .article-col__inner-image{height:100%;max-width:75%;flex-basis:75%}.dynamic-articles-row-component .article-col__inner-image img{width:100%;height:100%;object-fit:cover}@media (max-width:575px){.dynamic-articles-row-component .article-col__inner-text-wrapper{order:2;max-width:100%;flex-basis:100%}.dynamic-articles-row-component .article-col__inner-text-wrapper-introduction{font-size:12px;margin:15px 0 7px}.dynamic-articles-row-component .article-col__inner-text-wrapper-title{font-size:20px;line-height:27px;margin-bottom:7px}.dynamic-articles-row-component .article-col__inner-text-wrapper-description{font-size:14px;line-height:17px;display:-webkit-box;-webkit-line-clamp:5;-webkit-box-orient:vertical;overflow:hidden}.dynamic-articles-row-component .article-col__inner-image{order:1;max-width:100%;flex-basis:100%;height:auto}.dynamic-articles-row-component .article-col__inner-image>img{min-height:200px}}.dynamic-articles-row-component.article-type{width:100%}.dynamic-articles-row-component.article-type-12 .article-col{flex:0 1 100%}@media (max-width:575px){.dynamic-articles-row-component.article-type-12 .article-col{height:fit-content}.dynamic-articles-row-component.article-type-12 .article-col .article-col__inner{margin:12.5px}.dynamic-articles-row-component.article-type-12 .article-col .article-col__inner-text-wrapper-introduction{margin:7px 0}}.dynamic-articles-row-component.article-type-3 .article-col,.dynamic-articles-row-component.article-type-4 .article-col,.dynamic-articles-row-component.article-type-6 .article-col{flex:0 1 50%}.dynamic-articles-row-component.article-type-3 .article-col:first-child .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col:first-child .article-col__inner,.dynamic-articles-row-component.article-type-6 .article-col:first-child .article-col__inner{margin-right:12.5px}.dynamic-articles-row-component.article-type-3 .article-col:nth-child(2) .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col:nth-child(2) .article-col__inner,.dynamic-articles-row-component.article-type-6 .article-col:nth-child(2) .article-col__inner{margin-left:12.5px}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner-text-wrapper{order:2;max-width:100%;flex-basis:100%}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper-introduction,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper-introduction,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner-text-wrapper-introduction{margin:15px 0}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner-text-wrapper-description{display:-webkit-box;-webkit-line-clamp:2;-webkit-box-orient:vertical;overflow:hidden}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-image,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-image,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner-image{order:1;max-width:100%;height:400px;flex-basis:100%}@media (max-width:575px){.dynamic-articles-row-component.article-type-3 .article-col,.dynamic-articles-row-component.article-type-4 .article-col,.dynamic-articles-row-component.article-type-6 .article-col{height:fit-content;max-width:100%;flex-basis:100%}.dynamic-articles-row-component.article-type-3 .article-col:first-child .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col:first-child .article-col__inner,.dynamic-articles-row-component.article-type-6 .article-col:first-child .article-col__inner{margin:12.5px 12.5px 25px}.dynamic-articles-row-component.article-type-3 .article-col:nth-child(2) .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col:nth-child(2) .article-col__inner,.dynamic-articles-row-component.article-type-6 .article-col:nth-child(2) .article-col__inner{margin:25px 12.5px}.dynamic-articles-row-component.article-type-3 .article-col:last-child:after,.dynamic-articles-row-component.article-type-4 .article-col:last-child:after,.dynamic-articles-row-component.article-type-6 .article-col:last-child:after{display:none}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner{margin:12.5px}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner-text-wrapper{order:2;max-width:100%;flex-basis:100%}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper-introduction,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper-introduction,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner-text-wrapper-introduction{margin:7px 0}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner-text-wrapper-description{display:-webkit-box;-webkit-line-clamp:2;-webkit-box-orient:vertical;overflow:hidden}}@media (min-width:576px){.dynamic-articles-row-component.article-type-3 .article-col,.dynamic-articles-row-component.article-type-4 .article-col{flex:0 1 33.33%;max-width:33.33%}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner{margin:0 12.5px}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper{padding:0}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper-description{display:none}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper-title,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper-title{display:-webkit-box;-webkit-line-clamp:4;-webkit-box-orient:vertical;overflow:hidden;margin-bottom:0}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-image,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-image{width:400px;height:200px}.dynamic-articles-row-component.article-type-3 .article-col:first-child .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col:first-child .article-col__inner{margin-left:0}.dynamic-articles-row-component.article-type-3 .article-col:last-child .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col:last-child .article-col__inner{margin-right:0}}@media (max-width:575px){.dynamic-articles-row-component.article-type-3 .article-col,.dynamic-articles-row-component.article-type-4 .article-col{height:fit-content;max-width:100%;flex-basis:100%}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner{margin:25px 12.5px}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper-description{display:none}}@media (min-width:576px){.dynamic-articles-row-component.article-type-3 .article-col{flex:0 1 25%;max-width:25%}}.dynamic-articles-row-component.text-only-variant{justify-content:center}.dynamic-articles-row-component.text-only-variant.article-type{width:100%}.dynamic-articles-row-component.text-only-variant.article-type-12>.article-col{flex:0 1 50%}@media (max-width:575px){.dynamic-articles-row-component.text-only-variant.article-type-12>.article-col{flex:0 1 100%}}.dynamic-articles-row-component.text-only-variant.article-type-3 .article-col,.dynamic-articles-row-component.text-only-variant.article-type-4 .article-col,.dynamic-articles-row-component.text-only-variant.article-type-6 .article-col,.dynamic-articles-row-component.text-only-variant.article-type-12 .article-col{height:fit-content}.dynamic-articles-row-component.text-only-variant.article-type-3 .article-col__inner-image,.dynamic-articles-row-component.text-only-variant.article-type-4 .article-col__inner-image,.dynamic-articles-row-component.text-only-variant.article-type-6 .article-col__inner-image,.dynamic-articles-row-component.text-only-variant.article-type-12 .article-col__inner-image{display:none}.dynamic-articles-row-component.text-only-variant.article-type-3 .article-col__inner-text-wrapper,.dynamic-articles-row-component.text-only-variant.article-type-4 .article-col__inner-text-wrapper,.dynamic-articles-row-component.text-only-variant.article-type-6 .article-col__inner-text-wrapper,.dynamic-articles-row-component.text-only-variant.article-type-12 .article-col__inner-text-wrapper{height:100%;width:100%;max-width:100%;flex-basis:100%}.dynamic-articles-row-component.text-only-variant.article-type-3 .article-col__inner-text-wrapper-introduction,.dynamic-articles-row-component.text-only-variant.article-type-4 .article-col__inner-text-wrapper-introduction,.dynamic-articles-row-component.text-only-variant.article-type-6 .article-col__inner-text-wrapper-introduction,.dynamic-articles-row-component.text-only-variant.article-type-12 .article-col__inner-text-wrapper-introduction{margin-top:0}.dynamic-articles-row-component.text-only-variant.article-type-3 .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.text-only-variant.article-type-4 .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.text-only-variant.article-type-6 .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.text-only-variant.article-type-12 .article-col__inner-text-wrapper-description{display:-webkit-box;-webkit-line-clamp:2;-webkit-box-orient:vertical;overflow:hidden}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/grid/editors/ArticleListVinduetRow.vue?vue&type=template&id=9474c6c8&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_vm._ssrNode(_vm._ssrEscape("\n  "+_vm._s(_vm.viewModel)+"\n  ")+"<h2 class=\"dynamic-articles-row-component__header\">"+_vm._ssrEscape("\n    "+_vm._s(_vm.viewModel.headline)+"\n  ")+"</h2> <div"+(_vm._ssrClass(null,("dynamic-articles-row-component row article-type-" + (_vm.getArticleSize(_vm.viewModel.articles.data)) + " " + (_vm.viewModel.textOnlyVariant ? 'text-only-variant' : 'no-variant'))))+">"+(_vm._ssrList((_vm.viewModel.articles.data),function(article,index){return ("<div class=\"article-col\"><a"+(_vm._ssrAttr("href",article.url))+(_vm._ssrAttr("title",article.title))+" class=\"article-col__inner row\"><div class=\"article-col__inner-text-wrapper\"><div class=\"article-col__inner-text-wrapper-introduction\"><span>"+_vm._ssrEscape(_vm._s(article.genre)+". "+_vm._s(article.authorFirstName)+" "+_vm._s(article.authorLastName))+"</span></div> <div class=\"article-col__inner-text-wrapper-title\"><span>"+_vm._ssrEscape(_vm._s(article.title))+"</span></div> "+((article.ingress)?("<div class=\"article-col__inner-text-wrapper-description\"><div>"+(_vm._s(article.ingress))+"</div></div>"):"<!---->")+"</div> <div class=\"article-col__inner-image\">"+((article.image)?("<img"+(_vm._ssrAttr("data-image-src",article.image.url))+(_vm._ssrAttr("alt",article.image.alt))+">"):"<!---->")+"</div></a></div>")}))+"</div> <hr>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/grid/editors/ArticleListVinduetRow.vue?vue&type=template&id=9474c6c8&

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./mixins/GridEditorMixin.vue + 2 modules
var GridEditorMixin = __webpack_require__(110);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./components/common/GylImage.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};


let GylImagevue_type_script_lang_ts_default_1 = class default_1 extends nuxt_property_decorator_common["Vue"] {
  constructor() {
    super(...arguments);
    this.staticClass = '';
  }

  render(h) {
    if (!this.image) {
      return h();
    }

    let className = 'lazyload';

    if (this.$vnode && this.$vnode.data && this.$vnode.data.staticClass) {
      className += ` ${this.$vnode.data.staticClass}`;
    }

    const tag = 'img';

    if (this.isBackground) {
      className += ' lazyload-bg';
    }

    if (!this.sizes.length) {
      className += ' lazyload-measure';
    }

    const dataQuery = {
      mode: 'crop',
      center: '0.5%2C0.5'
    };

    if (this.imageHeight) {
      dataQuery.height = this.imageHeight;
    }

    if (this.image.focalPoint && this.image.focalPoint.top && this.image.focalPoint.left) {
      dataQuery.center = `${this.image.focalPoint.top}%2C${this.image.focalPoint.left}`;
    }

    this.staticClass = className;
    const attrs = {
      class: className,
      'data-src': this.url,
      'data-query-obj': JSON.stringify(dataQuery)
    };

    if (this.imageSizes) {
      attrs['data-sizes'] = 'auto';
      attrs['data-srcset'] = this.imageSizes;
    }

    if (this.heightRatio != null) {
      attrs['data-height-ratio'] = this.heightRatio;
    }

    if (this.image.altText) {
      attrs.alt = this.image.altText;
    }

    if (this.image.title) {
      attrs.title = this.image.title;
    }

    return h(tag, {
      attrs
    }, this.$slots.default);
  }

  mounted() {
    if (this.staticClass) {
      this.$el.setAttribute('class', this.staticClass);
    }
  }

  get url() {
    if (!this.image || !this.image.url) {
      return null;
    } // If the url contains remote.axd add an questionmark to make imageProcessor work
    // eslint-disable-next-line unicorn/prefer-includes


    if (this.image.url.indexOf('remote.axd') !== -1) {
      const query = this.sizes.length ? `width=${this.sizes[0].imageWidth}&quality=80` : 'quality=80';
      return `${this.image.url}?${query}`;
    }

    return `${this.image.url}?quality=80`;
  }

  get imageSizes() {
    if (!this.image) {
      return null;
    }

    const {
      image,
      heightRatio
    } = this;
    return this.sizes.reduce((arr, currentValue) => {
      const str = `${image.url}?width=${currentValue.imageWidth}&quality=80${heightRatio ? `&heightratio=${heightRatio}&mode=crop` : ''} ${currentValue.windowWidth ? `${currentValue.windowWidth}w` : ''}`;
      arr.push(str);
      return arr;
    }, []).join(', ');
  }

};

__decorate([Object(nuxt_property_decorator_common["Prop"])({
  default: false
})], GylImagevue_type_script_lang_ts_default_1.prototype, "isBackground", void 0);

__decorate([Object(nuxt_property_decorator_common["Prop"])()], GylImagevue_type_script_lang_ts_default_1.prototype, "image", void 0);

__decorate([Object(nuxt_property_decorator_common["Prop"])()], GylImagevue_type_script_lang_ts_default_1.prototype, "heightRatio", void 0);

__decorate([Object(nuxt_property_decorator_common["Prop"])()], GylImagevue_type_script_lang_ts_default_1.prototype, "imageHeight", void 0);

__decorate([Object(nuxt_property_decorator_common["Prop"])({
  default: () => {
    return [];
  }
})], GylImagevue_type_script_lang_ts_default_1.prototype, "sizes", void 0);

GylImagevue_type_script_lang_ts_default_1 = __decorate([Object(nuxt_property_decorator_common["Component"])({
  inheritAttrs: false
})], GylImagevue_type_script_lang_ts_default_1);
/* harmony default export */ var GylImagevue_type_script_lang_ts_ = (GylImagevue_type_script_lang_ts_default_1);
// CONCATENATED MODULE: ./components/common/GylImage.vue?vue&type=script&lang=ts&
 /* harmony default export */ var common_GylImagevue_type_script_lang_ts_ = (GylImagevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/common/GylImage.vue
var GylImage_render, GylImage_staticRenderFns


function injectStyles (context) {
  
  var style0 = __webpack_require__(138)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  common_GylImagevue_type_script_lang_ts_,
  GylImage_render,
  GylImage_staticRenderFns,
  false,
  injectStyles,
  "7279fd9a",
  "63ab06d2"
  
)

/* harmony default export */ var GylImage = (component.exports);
// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./components/grid/editors/ArticleListVinduetRow.vue?vue&type=script&lang=ts&
var ArticleListVinduetRowvue_type_script_lang_ts_decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};




let ArticleListVinduetRowvue_type_script_lang_ts_ArticleListVinduetRow = class ArticleListVinduetRow extends GridEditorMixin["a" /* default */] {
  getArticleSize(articles) {
    return 12 / articles.length;
  }

  get imageSizes() {
    let imageSizes;

    switch (this.viewModel.articles.length) {
      case 1:
        imageSizes = [{
          imageWidth: 150
        }];
        break;

      case 2:
        imageSizes = [{
          imageWidth: 150
        }];
        break;

      default:
        imageSizes = [{
          imageWidth: 300
        }];
        break;
    }

    return imageSizes;
  }

};
ArticleListVinduetRowvue_type_script_lang_ts_ArticleListVinduetRow = ArticleListVinduetRowvue_type_script_lang_ts_decorate([Object(nuxt_property_decorator_common["Component"])({
  components: {
    GylImage: GylImage
  }
})], ArticleListVinduetRowvue_type_script_lang_ts_ArticleListVinduetRow);
/* harmony default export */ var ArticleListVinduetRowvue_type_script_lang_ts_ = (ArticleListVinduetRowvue_type_script_lang_ts_ArticleListVinduetRow);
// CONCATENATED MODULE: ./components/grid/editors/ArticleListVinduetRow.vue?vue&type=script&lang=ts&
 /* harmony default export */ var editors_ArticleListVinduetRowvue_type_script_lang_ts_ = (ArticleListVinduetRowvue_type_script_lang_ts_); 
// CONCATENATED MODULE: ./components/grid/editors/ArticleListVinduetRow.vue



function ArticleListVinduetRow_injectStyles (context) {
  
  var style0 = __webpack_require__(140)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var ArticleListVinduetRow_component = Object(componentNormalizer["a" /* default */])(
  editors_ArticleListVinduetRowvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  ArticleListVinduetRow_injectStyles,
  null,
  "46e124b7"
  
)

/* harmony default export */ var editors_ArticleListVinduetRow = __webpack_exports__["default"] = (ArticleListVinduetRow_component.exports);

/***/ })

};;
//# sourceMappingURL=3.js.map