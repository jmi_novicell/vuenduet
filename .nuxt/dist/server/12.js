exports.ids = [12];
exports.modules = {

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./components/grid/GridRow.vue + 7 modules
var GridRow = __webpack_require__(22);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./mixins/Page.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let Pagevue_type_script_lang_ts_Page = class Page extends nuxt_property_decorator_common["Vue"] {
  get grid() {
    return this.content.gridContent || {};
  }

};

__decorate([Object(nuxt_property_decorator_common["Prop"])()], Pagevue_type_script_lang_ts_Page.prototype, "content", void 0);

Pagevue_type_script_lang_ts_Page = __decorate([Object(nuxt_property_decorator_common["Component"])({
  components: {
    GridRow: GridRow["a" /* default */]
  }
})], Pagevue_type_script_lang_ts_Page);
/* harmony default export */ var Pagevue_type_script_lang_ts_ = (Pagevue_type_script_lang_ts_Page);
// CONCATENATED MODULE: ./mixins/Page.vue?vue&type=script&lang=ts&
 /* harmony default export */ var mixins_Pagevue_type_script_lang_ts_ = (Pagevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./mixins/Page.vue
var render, staticRenderFns




/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  mixins_Pagevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "74be07c3"
  
)

/* harmony default export */ var mixins_Page = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 124:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(147);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(7).default
module.exports.__inject__ = function (context) {
  add("317532ee", content, false, context)
};

/***/ }),

/***/ 146:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_GenrePage_vue_vue_type_style_index_0_id_67a88aea_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(124);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_GenrePage_vue_vue_type_style_index_0_id_67a88aea_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_GenrePage_vue_vue_type_style_index_0_id_67a88aea_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_GenrePage_vue_vue_type_style_index_0_id_67a88aea_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_GenrePage_vue_vue_type_style_index_0_id_67a88aea_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 147:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "h2[data-v-67a88aea]{text-align:center;margin:50px 0 25px;font-size:28px;text-transform:uppercase}@media (min-width:992px){h2[data-v-67a88aea]{margin-bottom:50px}}.category-list__title[data-v-67a88aea]{display:none}@media (min-width:992px){.category-list__title[data-v-67a88aea]{display:flex}}.category-list__wrapper[data-v-67a88aea]{margin:25px 0 0;padding:0;list-style:none}@media (min-width:992px){.category-list__wrapper[data-v-67a88aea]{margin-top:50px}}.category-list__item[data-v-67a88aea]{padding-top:10px;padding-bottom:10px;border-bottom:1px solid #000}.category-list__item-link[data-v-67a88aea]{text-decoration:none;color:#000}.genre-wrapper[data-v-67a88aea]{display:none}@media (min-width:992px){.genre-wrapper[data-v-67a88aea]{display:flex}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./views/GenrePage.vue?vue&type=template&id=67a88aea&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"categorypage-page"},[_vm._ssrNode("<div class=\"content\" data-v-67a88aea>","</div>",[_vm._ssrNode("<div class=\"container\" data-v-67a88aea><div class=\"row\" data-v-67a88aea><h2 class=\"col-xs-12\" data-v-67a88aea>"+_vm._ssrEscape("\n          "+_vm._s(_vm.title)+"\n        ")+"</h2></div></div> "),_vm._ssrNode("<div id=\"normal-articles\" class=\"grid-items container\" data-v-67a88aea>","</div>",[_vm._ssrNode("<ul class=\"article-list__wrapper row\" data-v-67a88aea>","</ul>",_vm._l((_vm.articles),function(article,index){return _vm._ssrNode("<li class=\"article-list__item col-xs-12 col-md-3\" data-v-67a88aea>","</li>",[_c('ALink',{staticClass:"article-list__item-link row",attrs:{"to":article.url}},[(article.image)?_c('div',{staticClass:"col-xs-12 col-md-12"},[_c('figure',{staticClass:"image-wrapper"},[_c('img',{attrs:{"src":article.image}})])]):_vm._e(),_vm._v(" "),_c('div',{staticClass:"col-xs-12 col-md-12"},[_c('div',{staticClass:"info-wrapper"},[_vm._v("\n                "+_vm._s(article.genre)+". "+_vm._s(article.authorFirstName)+" "+_vm._s(article.authorLastName)+"\n              ")]),_vm._v(" "),_c('h4',{staticClass:"title-wrapper"},[_vm._v("\n                "+_vm._s(article.title)+"\n              ")]),_vm._v(" "),_c('div',{staticClass:"desc-wrapper",domProps:{"innerHTML":_vm._s(article.ingress)}})])])],1)}),0)])],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./views/GenrePage.vue?vue&type=template&id=67a88aea&scoped=true&

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./components/common/ALink.vue + 2 modules
var ALink = __webpack_require__(10);

// EXTERNAL MODULE: ./mixins/Page.vue + 2 modules
var Page = __webpack_require__(111);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./views/GenrePage.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};




let GenrePagevue_type_script_lang_ts_GenrePageTemplate = class GenrePageTemplate extends Page["a" /* default */] {
  get title() {
    return this.content.name;
  }

  get articles() {
    return this.content.articles.data;
  }

};

__decorate([Object(nuxt_property_decorator_common["Prop"])()], GenrePagevue_type_script_lang_ts_GenrePageTemplate.prototype, "content", void 0);

GenrePagevue_type_script_lang_ts_GenrePageTemplate = __decorate([Object(nuxt_property_decorator_common["Component"])({
  components: {
    ALink: ALink["a" /* default */]
  }
})], GenrePagevue_type_script_lang_ts_GenrePageTemplate);
/* harmony default export */ var GenrePagevue_type_script_lang_ts_ = (GenrePagevue_type_script_lang_ts_GenrePageTemplate);
// CONCATENATED MODULE: ./views/GenrePage.vue?vue&type=script&lang=ts&
 /* harmony default export */ var views_GenrePagevue_type_script_lang_ts_ = (GenrePagevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./views/GenrePage.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(146)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  views_GenrePagevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "67a88aea",
  "4ab330a4"
  
)

/* harmony default export */ var GenrePage = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=12.js.map