exports.ids = [6];
exports.modules = {

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.runtime.common.js
var vue_runtime_common = __webpack_require__(0);
var vue_runtime_common_default = /*#__PURE__*/__webpack_require__.n(vue_runtime_common);

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./mixins/GridEditorMixin.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



class GridEditorMixinvue_type_script_lang_ts_default_1 extends vue_runtime_common_default.a {}

__decorate([Object(nuxt_property_decorator_common["Prop"])({
  default: {}
})], GridEditorMixinvue_type_script_lang_ts_default_1.prototype, "viewModel", void 0);

__decorate([Object(nuxt_property_decorator_common["Prop"])({
  default: 12
})], GridEditorMixinvue_type_script_lang_ts_default_1.prototype, "size", void 0);
// CONCATENATED MODULE: ./mixins/GridEditorMixin.vue?vue&type=script&lang=ts&
 /* harmony default export */ var GridEditorMixinvue_type_script_lang_ts_ = (GridEditorMixinvue_type_script_lang_ts_default_1); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./mixins/GridEditorMixin.vue
var render, staticRenderFns




/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  GridEditorMixinvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "cb88482c"
  
)

/* harmony default export */ var GridEditorMixin = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 119:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(137);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(7).default
module.exports.__inject__ = function (context) {
  add("68a57a64", content, false, context)
};

/***/ }),

/***/ 136:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DynamicArticlesRow_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(119);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DynamicArticlesRow_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DynamicArticlesRow_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DynamicArticlesRow_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DynamicArticlesRow_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 137:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".dynamic-articles-row-component{margin:0;padding:0}.dynamic-articles-row-component .article-col a{color:inherit;text-decoration:none}.dynamic-articles-row-component .article-col__inner{height:100%;margin:0}.dynamic-articles-row-component .article-col__inner-text-wrapper{height:100%;max-width:25%;flex-basis:25%;padding-right:30px}.dynamic-articles-row-component .article-col__inner-text-wrapper-introduction{font-size:14px;margin-bottom:15px}.dynamic-articles-row-component .article-col__inner-text-wrapper-title{font-size:30px;line-height:35px;margin-bottom:15px}.dynamic-articles-row-component .article-col__inner-image{height:100%;max-width:75%;flex-basis:75%}.dynamic-articles-row-component .article-col__inner-image img{width:100%;height:100%;object-fit:cover}@media (max-width:575px){.dynamic-articles-row-component .article-col__inner-text-wrapper{order:2;max-width:100%;flex-basis:100%}.dynamic-articles-row-component .article-col__inner-text-wrapper-introduction{font-size:12px;margin:15px 0 7px}.dynamic-articles-row-component .article-col__inner-text-wrapper-title{font-size:20px;line-height:27px;margin-bottom:7px}.dynamic-articles-row-component .article-col__inner-text-wrapper-description{font-size:14px;line-height:17px;display:-webkit-box;-webkit-line-clamp:5;-webkit-box-orient:vertical;overflow:hidden}.dynamic-articles-row-component .article-col__inner-image{order:1;max-width:100%;flex-basis:100%;height:auto}.dynamic-articles-row-component .article-col__inner-image>img{min-height:200px}}.dynamic-articles-row-component.article-type{width:100%}.dynamic-articles-row-component.article-type-12 .article-col{flex:0 1 100%}@media (max-width:575px){.dynamic-articles-row-component.article-type-12 .article-col{height:fit-content}.dynamic-articles-row-component.article-type-12 .article-col .article-col__inner{margin:12.5px}.dynamic-articles-row-component.article-type-12 .article-col .article-col__inner-text-wrapper-introduction{margin:7px 0}}.dynamic-articles-row-component.article-type-3 .article-col,.dynamic-articles-row-component.article-type-4 .article-col,.dynamic-articles-row-component.article-type-6 .article-col{flex:0 1 50%}.dynamic-articles-row-component.article-type-3 .article-col:first-child .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col:first-child .article-col__inner,.dynamic-articles-row-component.article-type-6 .article-col:first-child .article-col__inner{margin-right:12.5px}.dynamic-articles-row-component.article-type-3 .article-col:nth-child(2) .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col:nth-child(2) .article-col__inner,.dynamic-articles-row-component.article-type-6 .article-col:nth-child(2) .article-col__inner{margin-left:12.5px}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner-text-wrapper{order:2;max-width:100%;flex-basis:100%}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper-introduction,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper-introduction,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner-text-wrapper-introduction{margin:15px 0}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner-text-wrapper-description{display:-webkit-box;-webkit-line-clamp:2;-webkit-box-orient:vertical;overflow:hidden}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-image,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-image,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner-image{order:1;max-width:100%;flex-basis:100%;height:auto}@media (max-width:575px){.dynamic-articles-row-component.article-type-3 .article-col,.dynamic-articles-row-component.article-type-4 .article-col,.dynamic-articles-row-component.article-type-6 .article-col{height:fit-content;max-width:100%;flex-basis:100%}.dynamic-articles-row-component.article-type-3 .article-col:first-child .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col:first-child .article-col__inner,.dynamic-articles-row-component.article-type-6 .article-col:first-child .article-col__inner{margin:12.5px 12.5px 25px}.dynamic-articles-row-component.article-type-3 .article-col:nth-child(2) .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col:nth-child(2) .article-col__inner,.dynamic-articles-row-component.article-type-6 .article-col:nth-child(2) .article-col__inner{margin:25px 12.5px}.dynamic-articles-row-component.article-type-3 .article-col:last-child:after,.dynamic-articles-row-component.article-type-4 .article-col:last-child:after,.dynamic-articles-row-component.article-type-6 .article-col:last-child:after{display:none}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner{margin:12.5px}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner-text-wrapper{order:2;max-width:100%;flex-basis:100%}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper-introduction,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper-introduction,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner-text-wrapper-introduction{margin:7px 0}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.article-type-6 .article-col .article-col__inner-text-wrapper-description{display:-webkit-box;-webkit-line-clamp:2;-webkit-box-orient:vertical;overflow:hidden}}@media (min-width:576px){.dynamic-articles-row-component.article-type-3 .article-col,.dynamic-articles-row-component.article-type-4 .article-col{flex:0 1 33.33%;max-width:33.33%}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner{margin:0 12.5px}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper{padding:0}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper-description{display:none}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper-title,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper-title{display:-webkit-box;-webkit-line-clamp:4;-webkit-box-orient:vertical;overflow:hidden;margin-bottom:0}.dynamic-articles-row-component.article-type-3 .article-col:first-child .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col:first-child .article-col__inner{margin-left:0}.dynamic-articles-row-component.article-type-3 .article-col:last-child .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col:last-child .article-col__inner{margin-right:0}}@media (max-width:575px){.dynamic-articles-row-component.article-type-3 .article-col,.dynamic-articles-row-component.article-type-4 .article-col{height:fit-content;max-width:100%;flex-basis:100%}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner{margin:25px 12.5px}.dynamic-articles-row-component.article-type-3 .article-col .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.article-type-4 .article-col .article-col__inner-text-wrapper-description{display:none}}@media (min-width:576px){.dynamic-articles-row-component.article-type-3 .article-col{flex:0 1 25%;max-width:25%}}.dynamic-articles-row-component.text-only-variant{justify-content:center}.dynamic-articles-row-component.text-only-variant.article-type{width:100%}.dynamic-articles-row-component.text-only-variant.article-type-12>.article-col{flex:0 1 50%}@media (max-width:575px){.dynamic-articles-row-component.text-only-variant.article-type-12>.article-col{flex:0 1 100%}}.dynamic-articles-row-component.text-only-variant.article-type-3 .article-col,.dynamic-articles-row-component.text-only-variant.article-type-4 .article-col,.dynamic-articles-row-component.text-only-variant.article-type-6 .article-col,.dynamic-articles-row-component.text-only-variant.article-type-12 .article-col{height:fit-content;height:auto}.dynamic-articles-row-component.text-only-variant.article-type-3 .article-col__inner-image,.dynamic-articles-row-component.text-only-variant.article-type-4 .article-col__inner-image,.dynamic-articles-row-component.text-only-variant.article-type-6 .article-col__inner-image,.dynamic-articles-row-component.text-only-variant.article-type-12 .article-col__inner-image{display:none}.dynamic-articles-row-component.text-only-variant.article-type-3 .article-col__inner-text-wrapper,.dynamic-articles-row-component.text-only-variant.article-type-4 .article-col__inner-text-wrapper,.dynamic-articles-row-component.text-only-variant.article-type-6 .article-col__inner-text-wrapper,.dynamic-articles-row-component.text-only-variant.article-type-12 .article-col__inner-text-wrapper{height:100%;width:100%;max-width:100%;flex-basis:100%}.dynamic-articles-row-component.text-only-variant.article-type-3 .article-col__inner-text-wrapper-introduction,.dynamic-articles-row-component.text-only-variant.article-type-4 .article-col__inner-text-wrapper-introduction,.dynamic-articles-row-component.text-only-variant.article-type-6 .article-col__inner-text-wrapper-introduction,.dynamic-articles-row-component.text-only-variant.article-type-12 .article-col__inner-text-wrapper-introduction{margin-top:0}.dynamic-articles-row-component.text-only-variant.article-type-3 .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.text-only-variant.article-type-4 .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.text-only-variant.article-type-6 .article-col__inner-text-wrapper-description,.dynamic-articles-row-component.text-only-variant.article-type-12 .article-col__inner-text-wrapper-description{display:-webkit-box;-webkit-line-clamp:2;-webkit-box-orient:vertical;overflow:hidden}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 167:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/grid/editors/DynamicArticlesRow.vue?vue&type=template&id=96315948&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:("dynamic-articles-row-component row article-type-" + (_vm.getArticleSize(_vm.viewModel.articles)) + " " + (_vm.viewModel.textOnlyVariant ? 'text-only-variant' : 'no-variant'))},[_vm._ssrNode((_vm._ssrList((_vm.viewModel.articles),function(article,index){return ("<div class=\"article-col\"><a"+(_vm._ssrAttr("href",article.link))+(_vm._ssrAttr("title",article.title))+" class=\"article-col__inner row\"><div class=\"article-col__inner-text-wrapper\"><div class=\"article-col__inner-text-wrapper-introduction\"><span>"+_vm._ssrEscape(_vm._s(article.introduction))+"</span></div> <div class=\"article-col__inner-text-wrapper-title\"><span>"+_vm._ssrEscape(_vm._s(article.title))+"</span></div> "+((article.description)?("<div class=\"article-col__inner-text-wrapper-description\"><span>"+_vm._ssrEscape(_vm._s(article.description))+"</span></div>"):"<!---->")+"</div> <div class=\"article-col__inner-image\">"+((article.image)?("<img"+(_vm._ssrAttr("data-image-src",article.image.url))+(_vm._ssrAttr("alt",article.image.alt))+">"):"<!---->")+"</div></a></div>")})))])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/grid/editors/DynamicArticlesRow.vue?vue&type=template&id=96315948&

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./mixins/GridEditorMixin.vue + 2 modules
var GridEditorMixin = __webpack_require__(110);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./components/grid/editors/DynamicArticlesRow.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let DynamicArticlesRowvue_type_script_lang_ts_DynamicArticlesRow = class DynamicArticlesRow extends GridEditorMixin["a" /* default */] {
  getArticleSize(articles) {
    return 12 / articles.length;
  }

};
DynamicArticlesRowvue_type_script_lang_ts_DynamicArticlesRow = __decorate([nuxt_property_decorator_common["Component"]], DynamicArticlesRowvue_type_script_lang_ts_DynamicArticlesRow);
/* harmony default export */ var DynamicArticlesRowvue_type_script_lang_ts_ = (DynamicArticlesRowvue_type_script_lang_ts_DynamicArticlesRow);
// CONCATENATED MODULE: ./components/grid/editors/DynamicArticlesRow.vue?vue&type=script&lang=ts&
 /* harmony default export */ var editors_DynamicArticlesRowvue_type_script_lang_ts_ = (DynamicArticlesRowvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/grid/editors/DynamicArticlesRow.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(136)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  editors_DynamicArticlesRowvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "8359c3ac"
  
)

/* harmony default export */ var editors_DynamicArticlesRow = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=6.js.map