exports.ids = [2,9];
exports.modules = {

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.runtime.common.js
var vue_runtime_common = __webpack_require__(0);
var vue_runtime_common_default = /*#__PURE__*/__webpack_require__.n(vue_runtime_common);

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./mixins/GridEditorMixin.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



class GridEditorMixinvue_type_script_lang_ts_default_1 extends vue_runtime_common_default.a {}

__decorate([Object(nuxt_property_decorator_common["Prop"])({
  default: {}
})], GridEditorMixinvue_type_script_lang_ts_default_1.prototype, "viewModel", void 0);

__decorate([Object(nuxt_property_decorator_common["Prop"])({
  default: 12
})], GridEditorMixinvue_type_script_lang_ts_default_1.prototype, "size", void 0);
// CONCATENATED MODULE: ./mixins/GridEditorMixin.vue?vue&type=script&lang=ts&
 /* harmony default export */ var GridEditorMixinvue_type_script_lang_ts_ = (GridEditorMixinvue_type_script_lang_ts_default_1); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./mixins/GridEditorMixin.vue
var render, staticRenderFns




/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  GridEditorMixinvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "cb88482c"
  
)

/* harmony default export */ var GridEditorMixin = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 112:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(114);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(7).default
module.exports.__inject__ = function (context) {
  add("24981e1c", content, false, context)
};

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Rte_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(112);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Rte_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Rte_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Rte_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Rte_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 114:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".grid-row.dark-bg-title{background-color:#000}.grid-row.dark-bg-title .rte{font-size:18px;text-align:center;font-weight:700;color:#fff}.grid-row.row-title .grid-column{padding-bottom:0}.grid-row.row-title .rte{text-align:center}.grid-row.row-title .rte h1,.grid-row.row-title .rte h2,.grid-row.row-title .rte h3,.grid-row.row-title .rte h4,.grid-row.row-title .rte h5{margin:0}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 117:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(133);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(7).default
module.exports.__inject__ = function (context) {
  add("57ded8c1", content, false, context)
};

/***/ }),

/***/ 118:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(135);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(7).default
module.exports.__inject__ = function (context) {
  add("12b31425", content, false, context)
};

/***/ }),

/***/ 127:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/grid/editors/Rte.vue?vue&type=template&id=88e51258&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_vm._ssrNode("<div class=\"rte\">"+(_vm._s(_vm.viewModel.text))+"</div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/grid/editors/Rte.vue?vue&type=template&id=88e51258&

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./mixins/GridEditorMixin.vue + 2 modules
var GridEditorMixin = __webpack_require__(110);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./components/grid/editors/Rte.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/* eslint-disable no-underscore-dangle, no-plusplus */




let Rtevue_type_script_lang_ts_Rte = class Rte extends GridEditorMixin["a" /* default */] {
  onChildChanged() {
    this.contentUpdated();
  }

  mounted() {
    this.$nextTick(this.addListeners);
  }

  beforeDestroy() {
    this.removeListeners();
  }

  navigate(event) {
    let {
      target
    } = event;
    let i = 0; // Go throught 5 parents max to find a tag

    while (i < 5 && !(target instanceof HTMLAnchorElement) && target.parentNode) {
      target = target.parentNode;
      i++;
    } // If target is still not a link, ignore


    if (!(target instanceof HTMLAnchorElement)) return;
    const href = target.getAttribute("href"); // Get link target, if local link, navigate with router link

    if (href && href[0] === "/") {
      event.preventDefault();
      this.$router.push(href);
    } else if (this.$ga) {
      // If Google Analytics is activated & is external link
      // https://developers.google.com/analytics/devguides/collection/analyticsjs/events
      this.$ga("send", "event", "Outbound Link", "click", target.href);
    }
  }

  contentUpdated() {
    this.removeListeners();
    this.$nextTick(() => {
      this.addListeners();
    });
  }

  addListeners() {
    this._links = this.$el.getElementsByTagName("a");

    for (let i = 0; i < this._links.length; i++) {
      this._links[i].addEventListener("click", this.navigate, false);
    }
  }

  removeListeners() {
    for (let i = 0; i < this._links.length; i++) {
      this._links[i].removeEventListener("click", this.navigate, false);
    }

    this._links = [];
  }

};

__decorate([Object(nuxt_property_decorator_common["Watch"])("viewModel.text")], Rtevue_type_script_lang_ts_Rte.prototype, "onChildChanged", null);

Rtevue_type_script_lang_ts_Rte = __decorate([Object(nuxt_property_decorator_common["Component"])({
  components: {}
})], Rtevue_type_script_lang_ts_Rte);
/* harmony default export */ var Rtevue_type_script_lang_ts_ = (Rtevue_type_script_lang_ts_Rte);
// CONCATENATED MODULE: ./components/grid/editors/Rte.vue?vue&type=script&lang=ts&
 /* harmony default export */ var editors_Rtevue_type_script_lang_ts_ = (Rtevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/grid/editors/Rte.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(113)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  editors_Rtevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "12de57f2"
  
)

/* harmony default export */ var editors_Rte = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 132:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Image_vue_vue_type_style_index_0_id_d55f0e6e_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(117);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Image_vue_vue_type_style_index_0_id_d55f0e6e_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Image_vue_vue_type_style_index_0_id_d55f0e6e_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Image_vue_vue_type_style_index_0_id_d55f0e6e_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Image_vue_vue_type_style_index_0_id_d55f0e6e_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 133:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "img[data-v-d55f0e6e]{width:100%;height:auto;max-height:100%;flex-shink:0;transition:opacity .2s ease-in}img.lazyload[data-v-d55f0e6e]:not([src]){opacity:0;visibility:hidden}img.lazyloaded[data-v-d55f0e6e]{opacity:1}img.lazyload-bg[data-v-d55f0e6e]{opacity:0;visibility:hidden;width:0;height:0}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 134:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TextAndImage_vue_vue_type_style_index_0_id_de6af196_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(118);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TextAndImage_vue_vue_type_style_index_0_id_de6af196_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TextAndImage_vue_vue_type_style_index_0_id_de6af196_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TextAndImage_vue_vue_type_style_index_0_id_de6af196_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TextAndImage_vue_vue_type_style_index_0_id_de6af196_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 135:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".text-and-image[data-v-de6af196]{justify-content:center;align-items:center;margin-bottom:30px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 153:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/grid/editors/TextAndImage.vue?vue&type=template&id=de6af196&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"text-and-image row"},[_vm._ssrNode("<div class=\"text-and-image__content col-xs-12 col-sm-6\" data-v-de6af196>","</div>",[_vm._ssrNode(((_vm.viewModel.headline)?("<h2 class=\"text-and-image__headline\" data-v-de6af196>"+_vm._ssrEscape("\n      "+_vm._s(_vm.viewModel.headline)+"\n    ")+"</h2>"):"<!---->")+" "),_c('ef-rte',{attrs:{"view-model":_vm.viewModel}})],2),_vm._ssrNode(" "+((_vm.viewModel.image)?("<div class=\"text-and-image__image col-xs-12 col-sm-6\" data-v-de6af196><Image"+(_vm._ssrAttr("image",_vm.viewModel.image))+(_vm._ssrAttr("height-ratio",0))+" data-v-de6af196></Image></div>"):"<!---->"))],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/grid/editors/TextAndImage.vue?vue&type=template&id=de6af196&scoped=true&

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./mixins/GridEditorMixin.vue + 2 modules
var GridEditorMixin = __webpack_require__(110);

// EXTERNAL MODULE: ./components/grid/editors/Rte.vue + 4 modules
var Rte = __webpack_require__(127);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./components/common/Image.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};


let Imagevue_type_script_lang_ts_Image = class Image extends nuxt_property_decorator_common["Vue"] {
  constructor() {
    super(...arguments);
    this.staticClass = '';
  }

  render(h) {
    if (!this.image) {
      return h();
    }

    let className = 'lazyload';

    if (this.$vnode.data && this.$vnode.data.staticClass) {
      className += ` ${this.$vnode.data.staticClass}`;
    }

    const tag = 'img';

    if (this.isBackground) {
      className += ' lazyload-bg';
    }

    if (!this.sizes.length) {
      className += ' lazyload-measure';
    }

    const dataQuery = {
      mode: 'crop',
      quality: '80',
      center: '0.5%2C0.5'
    };

    if (this.imageHeight) {
      dataQuery.height = this.imageHeight;
    }

    if (this.image.focalPoint && this.image.focalPoint.top && this.image.focalPoint.left) {
      dataQuery.center = `${this.image.focalPoint.top}%2C${this.image.focalPoint.left}`;
    }

    this.staticClass = className || '';
    const attrs = {
      class: className,
      'data-src': this.url,
      'data-query-obj': JSON.stringify(dataQuery)
    };

    if (this.imageSizes) {
      attrs['data-sizes'] = 'auto';
      attrs['data-srcset'] = this.imageSizes;
    }

    if (this.heightRatio != null) {
      attrs['data-height-ratio'] = this.heightRatio;
    }

    if (this.image.altText) {
      attrs.alt = this.image.altText;
    }

    if (this.image.title) {
      attrs.title = this.image.title;
    }

    return h(tag, {
      attrs
    }, this.$slots.default);
  }

  mounted() {
    this.$el.setAttribute('class', this.staticClass);
  }

  get url() {
    if (!this.image) {
      return null;
    }

    const {
      url
    } = this.image;
    return url;
  }

  get imageSizes() {
    if (!this.image) {
      return null;
    }

    const {
      url
    } = this.image;
    return this.sizes.reduce((arr, currentValue) => {
      const str = `${url}?width=${currentValue.imageWidth} ${currentValue.windowWidth ? `${currentValue.windowWidth}w` : ''}`;
      arr.push(str);
      return arr;
    }, []).join(', ');
  }

};

__decorate([Object(nuxt_property_decorator_common["Prop"])({
  default: false
})], Imagevue_type_script_lang_ts_Image.prototype, "isBackground", void 0);

__decorate([Object(nuxt_property_decorator_common["Prop"])()], Imagevue_type_script_lang_ts_Image.prototype, "image", void 0);

__decorate([Object(nuxt_property_decorator_common["Prop"])()], Imagevue_type_script_lang_ts_Image.prototype, "heightRatio", void 0);

__decorate([Object(nuxt_property_decorator_common["Prop"])()], Imagevue_type_script_lang_ts_Image.prototype, "imageHeight", void 0);

__decorate([Object(nuxt_property_decorator_common["Prop"])({
  default: () => []
})], Imagevue_type_script_lang_ts_Image.prototype, "sizes", void 0);

Imagevue_type_script_lang_ts_Image = __decorate([Object(nuxt_property_decorator_common["Component"])({
  inheritAttrs: false
})], Imagevue_type_script_lang_ts_Image);
/* harmony default export */ var Imagevue_type_script_lang_ts_ = (Imagevue_type_script_lang_ts_Image);
// CONCATENATED MODULE: ./components/common/Image.vue?vue&type=script&lang=ts&
 /* harmony default export */ var common_Imagevue_type_script_lang_ts_ = (Imagevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/common/Image.vue
var Image_render, Image_staticRenderFns


function injectStyles (context) {
  
  var style0 = __webpack_require__(132)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  common_Imagevue_type_script_lang_ts_,
  Image_render,
  Image_staticRenderFns,
  false,
  injectStyles,
  "d55f0e6e",
  "7424901a"
  
)

/* harmony default export */ var common_Image = (component.exports);
// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./components/grid/editors/TextAndImage.vue?vue&type=script&lang=ts&
var TextAndImagevue_type_script_lang_ts_decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};





let TextAndImagevue_type_script_lang_ts_TextAndImage = class TextAndImage extends GridEditorMixin["a" /* default */] {};
TextAndImagevue_type_script_lang_ts_TextAndImage = TextAndImagevue_type_script_lang_ts_decorate([Object(nuxt_property_decorator_common["Component"])({
  components: {
    Rte: Rte["default"],
    Image: common_Image
  }
})], TextAndImagevue_type_script_lang_ts_TextAndImage);
/* harmony default export */ var TextAndImagevue_type_script_lang_ts_ = (TextAndImagevue_type_script_lang_ts_TextAndImage);
// CONCATENATED MODULE: ./components/grid/editors/TextAndImage.vue?vue&type=script&lang=ts&
 /* harmony default export */ var editors_TextAndImagevue_type_script_lang_ts_ = (TextAndImagevue_type_script_lang_ts_); 
// CONCATENATED MODULE: ./components/grid/editors/TextAndImage.vue



function TextAndImage_injectStyles (context) {
  
  var style0 = __webpack_require__(134)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var TextAndImage_component = Object(componentNormalizer["a" /* default */])(
  editors_TextAndImagevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  TextAndImage_injectStyles,
  "de6af196",
  "0a69c91d"
  
)

/* harmony default export */ var editors_TextAndImage = __webpack_exports__["default"] = (TextAndImage_component.exports);

/***/ })

};;
//# sourceMappingURL=2.js.map