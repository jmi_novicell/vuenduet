exports.ids = [17];
exports.modules = {

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./components/grid/GridRow.vue + 7 modules
var GridRow = __webpack_require__(22);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./mixins/Page.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let Pagevue_type_script_lang_ts_Page = class Page extends nuxt_property_decorator_common["Vue"] {
  get grid() {
    return this.content.gridContent || {};
  }

};

__decorate([Object(nuxt_property_decorator_common["Prop"])()], Pagevue_type_script_lang_ts_Page.prototype, "content", void 0);

Pagevue_type_script_lang_ts_Page = __decorate([Object(nuxt_property_decorator_common["Component"])({
  components: {
    GridRow: GridRow["a" /* default */]
  }
})], Pagevue_type_script_lang_ts_Page);
/* harmony default export */ var Pagevue_type_script_lang_ts_ = (Pagevue_type_script_lang_ts_Page);
// CONCATENATED MODULE: ./mixins/Page.vue?vue&type=script&lang=ts&
 /* harmony default export */ var mixins_Pagevue_type_script_lang_ts_ = (Pagevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./mixins/Page.vue
var render, staticRenderFns




/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  mixins_Pagevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "74be07c3"
  
)

/* harmony default export */ var mixins_Page = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./views/FrontPage.vue?vue&type=template&id=1e8248e6&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"frontpage-page"},[_vm._ssrNode("<div class=\"content\" data-v-1e8248e6>","</div>",_vm._l((_vm.grid.rows),function(row,index){return _c('grid-row',{key:index,attrs:{"row":row}})}),1)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./views/FrontPage.vue?vue&type=template&id=1e8248e6&scoped=true&

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./mixins/Page.vue + 2 modules
var Page = __webpack_require__(111);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./views/FrontPage.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let FrontPagevue_type_script_lang_ts_FrontPageTemplate = class FrontPageTemplate extends Page["a" /* default */] {};
FrontPagevue_type_script_lang_ts_FrontPageTemplate = __decorate([Object(nuxt_property_decorator_common["Component"])({})], FrontPagevue_type_script_lang_ts_FrontPageTemplate);
/* harmony default export */ var FrontPagevue_type_script_lang_ts_ = (FrontPagevue_type_script_lang_ts_FrontPageTemplate);
// CONCATENATED MODULE: ./views/FrontPage.vue?vue&type=script&lang=ts&
 /* harmony default export */ var views_FrontPagevue_type_script_lang_ts_ = (FrontPagevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./views/FrontPage.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  views_FrontPagevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "1e8248e6",
  "782b9358"
  
)

/* harmony default export */ var FrontPage = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=17.js.map