exports.ids = [8];
exports.modules = {

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.runtime.common.js
var vue_runtime_common = __webpack_require__(0);
var vue_runtime_common_default = /*#__PURE__*/__webpack_require__.n(vue_runtime_common);

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./mixins/GridEditorMixin.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



class GridEditorMixinvue_type_script_lang_ts_default_1 extends vue_runtime_common_default.a {}

__decorate([Object(nuxt_property_decorator_common["Prop"])({
  default: {}
})], GridEditorMixinvue_type_script_lang_ts_default_1.prototype, "viewModel", void 0);

__decorate([Object(nuxt_property_decorator_common["Prop"])({
  default: 12
})], GridEditorMixinvue_type_script_lang_ts_default_1.prototype, "size", void 0);
// CONCATENATED MODULE: ./mixins/GridEditorMixin.vue?vue&type=script&lang=ts&
 /* harmony default export */ var GridEditorMixinvue_type_script_lang_ts_ = (GridEditorMixinvue_type_script_lang_ts_default_1); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./mixins/GridEditorMixin.vue
var render, staticRenderFns




/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  GridEditorMixinvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "cb88482c"
  
)

/* harmony default export */ var GridEditorMixin = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 122:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(143);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(7).default
module.exports.__inject__ = function (context) {
  add("b9429ca4", content, false, context)
};

/***/ }),

/***/ 142:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_KritikkRow_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(122);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_KritikkRow_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_KritikkRow_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_KritikkRow_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_KritikkRow_vue_vue_type_style_index_0_lang_postcss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 143:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".kritikk-row-component{margin:0;padding:0}.kritikk-row-component .kritikk-col{flex-basis:25%;max-width:25%;width:25%}.kritikk-row-component .kritikk-col a{color:inherit;text-decoration:none}.kritikk-row-component .kritikk-col__inner{height:inherit;margin:0 12.5px}.kritikk-row-component .kritikk-col__inner-text-wrapper{max-width:100%;flex-basis:100%;padding-right:30px}.kritikk-row-component .kritikk-col__inner-text-wrapper-introduction{font-size:14px;margin:15px 0}.kritikk-row-component .kritikk-col__inner-text-wrapper-title{font-size:17px;line-height:22px;margin-bottom:15px}.kritikk-row-component .kritikk-col__inner-image{max-width:100%;flex-basis:100%;background-color:#000;text-align:center;padding:5px}.kritikk-row-component .kritikk-col__inner-image img{width:auto;height:100%}@media (max-width:575px){.kritikk-row-component .kritikk-col{flex-basis:50%;max-width:50%;width:50%}.kritikk-row-component .kritikk-col__inner{height:fit-content;margin:0 12.5px}.kritikk-row-component .kritikk-col__inner-text-wrapper-introduction{font-size:12px;margin:10px 0 7px}.kritikk-row-component .kritikk-col__inner-text-wrapper-title{font-size:15px;line-height:20px;margin-bottom:20px}.kritikk-row-component .kritikk-col__inner-image{height:150px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 168:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/grid/editors/KritikkRow.vue?vue&type=template&id=10458576&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"kritikk-row-component row"},[_vm._ssrNode((_vm._ssrList((_vm.viewModel.articles),function(article,index){return ("<div class=\"kritikk-col\"><a"+(_vm._ssrAttr("href",article.link))+(_vm._ssrAttr("title",article.title))+" class=\"kritikk-col__inner row\"><div class=\"kritikk-col__inner-image\"><img"+(_vm._ssrAttr("data-image-src",article.image.url))+(_vm._ssrAttr("alt",article.image.alt))+"></div> <div class=\"kritikk-col__inner-text-wrapper\"><div class=\"kritikk-col__inner-text-wrapper-introduction\"><span>"+_vm._ssrEscape(_vm._s(article.introduction))+"</span></div> <div class=\"kritikk-col__inner-text-wrapper-title\"><span>"+_vm._ssrEscape(_vm._s(article.title))+"</span></div> <div class=\"kritikk-col__inner-text-wrapper-description\"><span>"+_vm._ssrEscape(_vm._s(article.description))+"</span></div></div></a></div>")})))])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/grid/editors/KritikkRow.vue?vue&type=template&id=10458576&

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./mixins/GridEditorMixin.vue + 2 modules
var GridEditorMixin = __webpack_require__(110);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./components/grid/editors/KritikkRow.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let KritikkRowvue_type_script_lang_ts_KritikkRow = class KritikkRow extends GridEditorMixin["a" /* default */] {};
KritikkRowvue_type_script_lang_ts_KritikkRow = __decorate([nuxt_property_decorator_common["Component"]], KritikkRowvue_type_script_lang_ts_KritikkRow);
/* harmony default export */ var KritikkRowvue_type_script_lang_ts_ = (KritikkRowvue_type_script_lang_ts_KritikkRow);
// CONCATENATED MODULE: ./components/grid/editors/KritikkRow.vue?vue&type=script&lang=ts&
 /* harmony default export */ var editors_KritikkRowvue_type_script_lang_ts_ = (KritikkRowvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/grid/editors/KritikkRow.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(142)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  editors_KritikkRowvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "d4b36822"
  
)

/* harmony default export */ var editors_KritikkRow = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=8.js.map