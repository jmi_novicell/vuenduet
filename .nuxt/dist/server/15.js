exports.ids = [15];
exports.modules = {

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./components/grid/GridRow.vue + 7 modules
var GridRow = __webpack_require__(22);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./mixins/Page.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let Pagevue_type_script_lang_ts_Page = class Page extends nuxt_property_decorator_common["Vue"] {
  get grid() {
    return this.content.gridContent || {};
  }

};

__decorate([Object(nuxt_property_decorator_common["Prop"])()], Pagevue_type_script_lang_ts_Page.prototype, "content", void 0);

Pagevue_type_script_lang_ts_Page = __decorate([Object(nuxt_property_decorator_common["Component"])({
  components: {
    GridRow: GridRow["a" /* default */]
  }
})], Pagevue_type_script_lang_ts_Page);
/* harmony default export */ var Pagevue_type_script_lang_ts_ = (Pagevue_type_script_lang_ts_Page);
// CONCATENATED MODULE: ./mixins/Page.vue?vue&type=script&lang=ts&
 /* harmony default export */ var mixins_Pagevue_type_script_lang_ts_ = (Pagevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./mixins/Page.vue
var render, staticRenderFns




/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  mixins_Pagevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "74be07c3"
  
)

/* harmony default export */ var mixins_Page = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./views/ArchivePage.vue?vue&type=template&id=000d0c06&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"archivepage-page"},[_vm._ssrNode("<div class=\"content\" data-v-000d0c06><div class=\"container\" data-v-000d0c06><div class=\"row\" data-v-000d0c06><h2 class=\"col-xs-12\" data-v-000d0c06>"+_vm._ssrEscape("\n          "+_vm._s(_vm.title)+"\n        ")+"</h2></div> <div id=\"archived-articles\" data-onapp-init-v-controller=\"gridHeadlessContentController\" data-api-endpoint-url=\"http://gyldendalstagingapi.azurewebsites.net\""+(_vm._ssrAttr("data-pagination-size",_vm.paginationSize))+" class=\"grid-items\" data-v-000d0c06><div class=\"archive-list__title\" data-v-000d0c06><div class=\"col-md-2\" data-v-000d0c06><h4 data-v-000d0c06><a href=\"javascript://\" class=\"published-order\" data-v-000d0c06>PUBLISERT</a></h4></div> <div class=\"col-md-10\" data-v-000d0c06><div class=\"row\" data-v-000d0c06><div class=\"col-md-4\" data-v-000d0c06><h4 data-v-000d0c06><a href=\"javascript://\" class=\"author-order\" data-v-000d0c06>FORFATTER (A–Å)</a></h4></div> <div class=\"col-md-6\" data-v-000d0c06><h4 data-v-000d0c06><a href=\"javascript://\" class=\"title-order\" data-v-000d0c06>TITTEL (A–Å)</a></h4></div> <div class=\"col-md-2\" data-v-000d0c06><h4 data-v-000d0c06><a href=\"javascript://\" class=\"genre-order\" data-v-000d0c06>SJANGER</a></h4></div></div></div></div> <ul class=\"archive-list__wrapper row\" data-v-000d0c06><span data-v-000d0c06>Loading articles</span></ul> <button class=\"more-items\" data-v-000d0c06>\n          Flere artikler\n        </button></div></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./views/ArchivePage.vue?vue&type=template&id=000d0c06&scoped=true&

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./mixins/Page.vue + 2 modules
var Page = __webpack_require__(111);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./views/ArchivePage.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let ArchivePagevue_type_script_lang_ts_ArchivePageTemplate = class ArchivePageTemplate extends Page["a" /* default */] {
  get title() {
    return this.content.titleArchive;
  }

  get dataItems() {
    const dataItemsURL = `${process.env.API_URL}/api/archive`;
    return dataItemsURL;
  }

  get paginationSize() {
    const paginationSize = "{\"mobile\":4,\"desktop\":4}";
    return paginationSize;
  }

};

__decorate([Object(nuxt_property_decorator_common["Prop"])()], ArchivePagevue_type_script_lang_ts_ArchivePageTemplate.prototype, "content", void 0);

ArchivePagevue_type_script_lang_ts_ArchivePageTemplate = __decorate([nuxt_property_decorator_common["Component"]], ArchivePagevue_type_script_lang_ts_ArchivePageTemplate);
/* harmony default export */ var ArchivePagevue_type_script_lang_ts_ = (ArchivePagevue_type_script_lang_ts_ArchivePageTemplate);
// CONCATENATED MODULE: ./views/ArchivePage.vue?vue&type=script&lang=ts&
 /* harmony default export */ var views_ArchivePagevue_type_script_lang_ts_ = (ArchivePagevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./views/ArchivePage.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  views_ArchivePagevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "000d0c06",
  "8bb77226"
  
)

/* harmony default export */ var ArchivePage = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=15.js.map