exports.ids = [16];
exports.modules = {

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./components/grid/GridRow.vue + 7 modules
var GridRow = __webpack_require__(22);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./mixins/Page.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let Pagevue_type_script_lang_ts_Page = class Page extends nuxt_property_decorator_common["Vue"] {
  get grid() {
    return this.content.gridContent || {};
  }

};

__decorate([Object(nuxt_property_decorator_common["Prop"])()], Pagevue_type_script_lang_ts_Page.prototype, "content", void 0);

Pagevue_type_script_lang_ts_Page = __decorate([Object(nuxt_property_decorator_common["Component"])({
  components: {
    GridRow: GridRow["a" /* default */]
  }
})], Pagevue_type_script_lang_ts_Page);
/* harmony default export */ var Pagevue_type_script_lang_ts_ = (Pagevue_type_script_lang_ts_Page);
// CONCATENATED MODULE: ./mixins/Page.vue?vue&type=script&lang=ts&
 /* harmony default export */ var mixins_Pagevue_type_script_lang_ts_ = (Pagevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./mixins/Page.vue
var render, staticRenderFns




/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  mixins_Pagevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "74be07c3"
  
)

/* harmony default export */ var mixins_Page = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./views/AuthorsPage.vue?vue&type=template&id=7e1c8fa3&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"authors-list-page"},[_vm._ssrNode("<div class=\"content\" data-v-7e1c8fa3><div class=\"container\" data-v-7e1c8fa3><div class=\"row\" data-v-7e1c8fa3><h2 class=\"col-xs-12\" data-v-7e1c8fa3>"+_vm._ssrEscape("\n          "+_vm._s(_vm.title)+"\n        ")+"</h2></div> <div id=\"authors-items\" data-onapp-init-v-controller=\"gridHeadlessContentController\""+(_vm._ssrAttr("data-api-endpoint-url",_vm.dataItems))+(_vm._ssrAttr("data-pagination-size",_vm.paginationSize))+" class=\"grid-items container\" data-v-7e1c8fa3><ul class=\"authors-list__wrapper row\" data-v-7e1c8fa3><span data-v-7e1c8fa3>Loading authors</span></ul> <button class=\"more-items\" data-v-7e1c8fa3>\n          Flere forfattere\n        </button></div></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./views/AuthorsPage.vue?vue&type=template&id=7e1c8fa3&scoped=true&

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./mixins/Page.vue + 2 modules
var Page = __webpack_require__(111);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./views/AuthorsPage.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let AuthorsPagevue_type_script_lang_ts_AuthorsPageTemplate = class AuthorsPageTemplate extends Page["a" /* default */] {
  get title() {
    return this.content.titleAuthors;
  }

  get dataItems() {
    const dataItemsURL = `${process.env.API_URL}/api/authors`;
    return dataItemsURL;
  }

  get paginationSize() {
    const paginationSize = "{\"mobile\":4,\"desktop\":4}";
    return paginationSize;
  }

};

__decorate([Object(nuxt_property_decorator_common["Prop"])()], AuthorsPagevue_type_script_lang_ts_AuthorsPageTemplate.prototype, "content", void 0);

AuthorsPagevue_type_script_lang_ts_AuthorsPageTemplate = __decorate([nuxt_property_decorator_common["Component"]], AuthorsPagevue_type_script_lang_ts_AuthorsPageTemplate);
/* harmony default export */ var AuthorsPagevue_type_script_lang_ts_ = (AuthorsPagevue_type_script_lang_ts_AuthorsPageTemplate);
// CONCATENATED MODULE: ./views/AuthorsPage.vue?vue&type=script&lang=ts&
 /* harmony default export */ var views_AuthorsPagevue_type_script_lang_ts_ = (AuthorsPagevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./views/AuthorsPage.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  views_AuthorsPagevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "7e1c8fa3",
  "0334df1a"
  
)

/* harmony default export */ var AuthorsPage = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=16.js.map