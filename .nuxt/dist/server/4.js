exports.ids = [4];
exports.modules = {

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./components/grid/GridRow.vue + 7 modules
var GridRow = __webpack_require__(22);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./mixins/Page.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let Pagevue_type_script_lang_ts_Page = class Page extends nuxt_property_decorator_common["Vue"] {
  get grid() {
    return this.content.gridContent || {};
  }

};

__decorate([Object(nuxt_property_decorator_common["Prop"])()], Pagevue_type_script_lang_ts_Page.prototype, "content", void 0);

Pagevue_type_script_lang_ts_Page = __decorate([Object(nuxt_property_decorator_common["Component"])({
  components: {
    GridRow: GridRow["a" /* default */]
  }
})], Pagevue_type_script_lang_ts_Page);
/* harmony default export */ var Pagevue_type_script_lang_ts_ = (Pagevue_type_script_lang_ts_Page);
// CONCATENATED MODULE: ./mixins/Page.vue?vue&type=script&lang=ts&
 /* harmony default export */ var mixins_Pagevue_type_script_lang_ts_ = (Pagevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./mixins/Page.vue
var render, staticRenderFns




/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  mixins_Pagevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "74be07c3"
  
)

/* harmony default export */ var mixins_Page = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 125:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(149);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(7).default
module.exports.__inject__ = function (context) {
  add("cbdb868e", content, false, context)
};

/***/ }),

/***/ 148:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SocialLinks_vue_vue_type_style_index_0_id_2bac3a90_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(125);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SocialLinks_vue_vue_type_style_index_0_id_2bac3a90_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SocialLinks_vue_vue_type_style_index_0_id_2bac3a90_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SocialLinks_vue_vue_type_style_index_0_id_2bac3a90_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_4_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_4_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_nuxt_webpack_node_modules_postcss_loader_src_index_js_ref_4_oneOf_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SocialLinks_vue_vue_type_style_index_0_id_2bac3a90_lang_postcss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 149:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".social-links__wrapper[data-v-2bac3a90]{list-style-type:none;margin:0;padding:0}.social-links__item+.social-links__item[data-v-2bac3a90]{margin-top:15px}.social-links__icon[data-v-2bac3a90]{width:50px;height:50px;max-width:inherit;display:inline-block}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./views/ArticlePage.vue?vue&type=template&id=f47b33ac&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"articlepage-page"},[_vm._ssrNode("<div class=\"content\" data-v-f47b33ac>","</div>",[_vm._ssrNode("<div class=\"container header-title\" data-v-f47b33ac>","</div>",[_vm._ssrNode("<div class=\"row\" data-v-f47b33ac>","</div>",[_vm._ssrNode("<div class=\"col-md-3 hide-mobile\" data-v-f47b33ac>","</div>",[_vm._ssrNode(_vm._ssrEscape("\n          "+_vm._s(_vm.genre)+".\n          ")+((_vm.authors && _vm.authors.length > 0)?("<div data-v-f47b33ac>"+(_vm._ssrList((_vm.authors),function(author,index){return ("<div data-v-f47b33ac>"+_vm._ssrEscape(_vm._s(author.firstName)+" "+_vm._s(author.lastName))+"</div>")}))+"</div>"):"<!---->")+" "+((_vm.publishedDate)?("<div data-v-f47b33ac>"+_vm._ssrEscape("\n            "+_vm._s(_vm.publishedDate)+"\n          ")+"</div>"):("<div data-v-f47b33ac>"+_vm._ssrEscape("\n            "+_vm._s(_vm.publishedNumber)+"\n          ")+"</div>"))+" <div data-v-f47b33ac>"+(_vm._s(_vm.factboxLeft))+"</div> "),_vm._ssrNode("<div class=\"header-social-networks\" data-v-f47b33ac>","</div>",[_c('social-links',{attrs:{"items":_vm.socialShares}})],1)],2),_vm._ssrNode(" <div class=\"col-xs-12 col-md-6\" data-v-f47b33ac><h1 data-v-f47b33ac>"+_vm._ssrEscape(_vm._s(_vm.articleTitle))+"</h1> <h6 class=\"summary\" data-v-f47b33ac>"+(_vm._s(_vm.ingress))+"</h6></div> <div class=\"col-md-3 hide-mobile\" data-v-f47b33ac><div data-v-f47b33ac>"+(_vm._s(_vm.factboxRight))+"</div> <p data-v-f47b33ac>"+_vm._ssrEscape("\n            "+_vm._s(_vm.extraData)+"\n          ")+"</p></div>")],2),_vm._ssrNode(" <div class=\"row hide-desktop\" data-v-f47b33ac><div class=\"col-xs-6 info-data-wrapper\" data-v-f47b33ac>"+_vm._ssrEscape("\n          "+_vm._s(_vm.genre)+".\n          ")+((_vm.authors && _vm.authors.length > 0)?("<div data-v-f47b33ac>"+(_vm._ssrList((_vm.authors),function(author,index){return ("<div data-v-f47b33ac>"+_vm._ssrEscape(_vm._s(author.firstName)+" "+_vm._s(author.lastName))+"</div>")}))+"</div>"):"<!---->")+" "+((_vm.publishedDate)?("<div data-v-f47b33ac>"+_vm._ssrEscape("\n            "+_vm._s(_vm.publishedDate)+"\n          ")+"</div>"):("<div data-v-f47b33ac>"+_vm._ssrEscape("\n            "+_vm._s(_vm.publishedNumber)+"\n          ")+"</div>"))+"</div> <div class=\"col-xs-6\" data-v-f47b33ac><div data-v-f47b33ac>"+(_vm._s(_vm.factboxRight))+"</div> <p data-v-f47b33ac>"+_vm._ssrEscape("\n            "+_vm._s(_vm.extraData)+"\n          ")+"</p></div></div>")],2),_vm._ssrNode(" "+((_vm.featuredImage)?("<div class=\"container\" data-v-f47b33ac><div class=\"row center-xs\" data-v-f47b33ac><div class=\"col-xs-12 col-md-8\" data-v-f47b33ac><figure class=\"featured-image-wrapper\" data-v-f47b33ac><img"+(_vm._ssrAttr("src",_vm.featuredImage.url))+(_vm._ssrAttr("alt",_vm.featuredImage.altText))+" data-v-f47b33ac> <figcaption class=\"center-xs\" data-v-f47b33ac><div data-v-f47b33ac>"+(_vm._s(_vm.featuredImage.title))+"</div></figcaption></figure></div></div></div>"):"<!---->")+" "),_vm._ssrNode("<div class=\"container grid-wrapper\" data-v-f47b33ac>","</div>",[_vm._ssrNode("<div class=\"row bottom-xs\" data-v-f47b33ac>","</div>",[_vm._ssrNode("<div class=\"col-md-3 hide-mobile\" data-v-f47b33ac>","</div>",[_c('social-links',{attrs:{"items":_vm.socialShares}})],1),_vm._ssrNode(" <div class=\"col-xs-12 col-md-6\" data-v-f47b33ac><div data-v-f47b33ac>"+(_vm._s(_vm.body))+"</div></div> "),_vm._ssrNode("<div class=\"col-xs-12 col-md-3\" data-v-f47b33ac>","</div>",[_vm._ssrNode("<div class=\"row\" data-v-f47b33ac>","</div>",[_vm._ssrNode("<div class=\"col-xs-6 hide-desktop\" data-v-f47b33ac>","</div>",[_c('social-links',{attrs:{"items":_vm.socialShares}})],1),_vm._ssrNode(" <div class=\"col-xs-6 col-md-12\" data-v-f47b33ac>"+((_vm.authors && _vm.authors.length > 0)?("<div data-v-f47b33ac>"+(_vm._ssrList((_vm.authors),function(author,index){return ("<div data-v-f47b33ac>"+_vm._ssrEscape("\n                  "+_vm._s(author.firstName)+"\n                  ")+"<div data-v-f47b33ac>"+(_vm._s(author.bio))+"</div></div>")}))+"</div>"):"<!---->")+"</div>")],2)])],2)]),_vm._ssrNode(" "+((_vm.footNotes)?("<div data-onapp-init-v-controller=\"footnotesComponentController\" class=\"footnotes-component desktop-view\" data-v-f47b33ac><div class=\"footnotes-component__container\" data-v-f47b33ac>"+(_vm._ssrList((_vm.footNotes),function(note,index){return ("<div data-v-f47b33ac><div"+(_vm._ssrAttr("data-footnote-id",note.id))+" class=\"footnotes-component__note\" data-v-f47b33ac><span data-v-f47b33ac>"+_vm._ssrEscape(_vm._s(note.id)+". "+_vm._s(note.text))+"</span></div></div>")}))+"</div></div>"):"<!---->")+" "),_c('newsletter-cta',{attrs:{"cta-text":_vm.ctaText}})],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./views/ArticlePage.vue?vue&type=template&id=f47b33ac&scoped=true&

// EXTERNAL MODULE: ./node_modules/nuxt-property-decorator/lib/nuxt-property-decorator.common.js
var nuxt_property_decorator_common = __webpack_require__(1);

// EXTERNAL MODULE: ./mixins/Page.vue + 2 modules
var Page = __webpack_require__(111);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/article/SocialLinks.vue?vue&type=template&id=2bac3a90&scoped=true&
var SocialLinksvue_type_template_id_2bac3a90_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('nav',{staticClass:"social-links"},[_vm._ssrNode("<ul class=\"social-links__wrapper\" data-v-2bac3a90>","</ul>",_vm._l((_vm.items),function(social,index){return _vm._ssrNode("<li class=\"social-links__item\" data-v-2bac3a90>","</li>",[_vm._ssrNode("<a"+(_vm._ssrAttr("href",social.url))+(_vm._ssrAttr("title",social.name))+(_vm._ssrAttr("target",social.target))+" class=\"social-links__link\" data-v-2bac3a90>","</a>",[_c('svg-icon',{staticClass:"social-links__icon",attrs:{"name":social.type}})],1)])}),0)])}
var SocialLinksvue_type_template_id_2bac3a90_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./components/article/SocialLinks.vue?vue&type=template&id=2bac3a90&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./components/article/SocialLinks.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};


let SocialLinksvue_type_script_lang_ts_default_1 = class default_1 extends nuxt_property_decorator_common["Vue"] {};

__decorate([Object(nuxt_property_decorator_common["Prop"])({
  default: []
})], SocialLinksvue_type_script_lang_ts_default_1.prototype, "items", void 0);

SocialLinksvue_type_script_lang_ts_default_1 = __decorate([Object(nuxt_property_decorator_common["Component"])({})], SocialLinksvue_type_script_lang_ts_default_1);
/* harmony default export */ var SocialLinksvue_type_script_lang_ts_ = (SocialLinksvue_type_script_lang_ts_default_1);
// CONCATENATED MODULE: ./components/article/SocialLinks.vue?vue&type=script&lang=ts&
 /* harmony default export */ var article_SocialLinksvue_type_script_lang_ts_ = (SocialLinksvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/article/SocialLinks.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(148)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  article_SocialLinksvue_type_script_lang_ts_,
  SocialLinksvue_type_template_id_2bac3a90_scoped_true_render,
  SocialLinksvue_type_template_id_2bac3a90_scoped_true_staticRenderFns,
  false,
  injectStyles,
  "2bac3a90",
  "72894734"
  
)

/* harmony default export */ var SocialLinks = (component.exports);
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/common/NewsletterCta.vue?vue&type=template&id=616c4c84&scoped=true&
var NewsletterCtavue_type_template_id_616c4c84_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-fluid"},[_vm._ssrNode("<div class=\"grid-row\" data-v-616c4c84><div class=\"grid-row__background\" style=\"background-color:#000;\" data-v-616c4c84><div class=\"container\" data-v-616c4c84><div class=\"row\" data-v-616c4c84><div class=\"grid-column col-xs-12 col-ms-12\" data-v-616c4c84><div data-v-616c4c84><div class=\"rte\" data-v-616c4c84><h3 class=\"newsletter-link\" data-v-616c4c84>"+(_vm._s(_vm.ctaText))+"</h3></div></div></div></div></div></div></div>")])}
var NewsletterCtavue_type_template_id_616c4c84_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./components/common/NewsletterCta.vue?vue&type=template&id=616c4c84&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./components/common/NewsletterCta.vue?vue&type=script&lang=ts&
var NewsletterCtavue_type_script_lang_ts_decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};


let NewsletterCtavue_type_script_lang_ts_NewsletterCta = class NewsletterCta extends nuxt_property_decorator_common["Vue"] {};

NewsletterCtavue_type_script_lang_ts_decorate([Object(nuxt_property_decorator_common["Prop"])()], NewsletterCtavue_type_script_lang_ts_NewsletterCta.prototype, "ctaText", void 0);

NewsletterCtavue_type_script_lang_ts_NewsletterCta = NewsletterCtavue_type_script_lang_ts_decorate([nuxt_property_decorator_common["Component"]], NewsletterCtavue_type_script_lang_ts_NewsletterCta);
/* harmony default export */ var NewsletterCtavue_type_script_lang_ts_ = (NewsletterCtavue_type_script_lang_ts_NewsletterCta);
// CONCATENATED MODULE: ./components/common/NewsletterCta.vue?vue&type=script&lang=ts&
 /* harmony default export */ var common_NewsletterCtavue_type_script_lang_ts_ = (NewsletterCtavue_type_script_lang_ts_); 
// CONCATENATED MODULE: ./components/common/NewsletterCta.vue



function NewsletterCta_injectStyles (context) {
  
  
}

/* normalize component */

var NewsletterCta_component = Object(componentNormalizer["a" /* default */])(
  common_NewsletterCtavue_type_script_lang_ts_,
  NewsletterCtavue_type_template_id_616c4c84_scoped_true_render,
  NewsletterCtavue_type_template_id_616c4c84_scoped_true_staticRenderFns,
  false,
  NewsletterCta_injectStyles,
  "616c4c84",
  "681ef056"
  
)

/* harmony default export */ var common_NewsletterCta = (NewsletterCta_component.exports);
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/article/RelatedArticles.vue?vue&type=template&id=0f4e9509&scoped=true&
var RelatedArticlesvue_type_template_id_0f4e9509_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"grid-items container",attrs:{"id":"related-articles","data-onapp-init-v-controller":"gridHeadlessContentController","data-api-endpoint-url":_vm.dataItems,"data-pagination-size":_vm.paginationSize,"data-filters-parameter":_vm.filterParameter}},[_vm._ssrNode("<ul class=\"article-list__wrapper row\" data-v-0f4e9509></ul> <button class=\"button-pinned-articles\" data-v-0f4e9509>\n    Flere artikler\n  </button>")])}
var RelatedArticlesvue_type_template_id_0f4e9509_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./components/article/RelatedArticles.vue?vue&type=template&id=0f4e9509&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./components/article/RelatedArticles.vue?vue&type=script&lang=ts&
var RelatedArticlesvue_type_script_lang_ts_decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};


let RelatedArticlesvue_type_script_lang_ts_default_1 = class default_1 extends nuxt_property_decorator_common["Vue"] {
  get dataItems() {
    const dataItemsURL = `${process.env.API_URL}/api/articles`;
    return dataItemsURL;
  }

  get paginationSize() {
    const paginationSize = "{\"mobile\":4,\"desktop\":4}";
    return paginationSize;
  }

  get filterParameter() {
    const filterParameter = `{"relatedArticles": {"param": "articles_id", "value": "${this.articlesId.join()}"}}`;
    return filterParameter;
  }

};

RelatedArticlesvue_type_script_lang_ts_decorate([Object(nuxt_property_decorator_common["Prop"])()], RelatedArticlesvue_type_script_lang_ts_default_1.prototype, "articles", void 0);

RelatedArticlesvue_type_script_lang_ts_decorate([Object(nuxt_property_decorator_common["Prop"])()], RelatedArticlesvue_type_script_lang_ts_default_1.prototype, "articlesId", void 0);

RelatedArticlesvue_type_script_lang_ts_default_1 = RelatedArticlesvue_type_script_lang_ts_decorate([nuxt_property_decorator_common["Component"]], RelatedArticlesvue_type_script_lang_ts_default_1);
/* harmony default export */ var RelatedArticlesvue_type_script_lang_ts_ = (RelatedArticlesvue_type_script_lang_ts_default_1);
// CONCATENATED MODULE: ./components/article/RelatedArticles.vue?vue&type=script&lang=ts&
 /* harmony default export */ var article_RelatedArticlesvue_type_script_lang_ts_ = (RelatedArticlesvue_type_script_lang_ts_); 
// CONCATENATED MODULE: ./components/article/RelatedArticles.vue



function RelatedArticles_injectStyles (context) {
  
  
}

/* normalize component */

var RelatedArticles_component = Object(componentNormalizer["a" /* default */])(
  article_RelatedArticlesvue_type_script_lang_ts_,
  RelatedArticlesvue_type_template_id_0f4e9509_scoped_true_render,
  RelatedArticlesvue_type_template_id_0f4e9509_scoped_true_staticRenderFns,
  false,
  RelatedArticles_injectStyles,
  "0f4e9509",
  "71c060a2"
  
)

/* harmony default export */ var RelatedArticles = (RelatedArticles_component.exports);
// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vue-loader/lib??vue-loader-options!./views/ArticlePage.vue?vue&type=script&lang=ts&
var ArticlePagevue_type_script_lang_ts_decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};






let ArticlePagevue_type_script_lang_ts_ArticlePageTemplate = class ArticlePageTemplate extends Page["a" /* default */] {
  get articleTitle() {
    return this.content.title;
  }

  get body() {
    return this.content.body;
  }

  get ingress() {
    return this.content.ingress;
  }

  get genre() {
    return this.content.genre;
  }

  get authors() {
    return this.content.author.data;
  }

  get publishedDate() {
    return this.content.publishedInfo;
  }

  get publishedNumber() {
    return this.content.publishedNumber;
  }

  get factboxLeft() {
    return this.content.factboxLeft;
  }

  get factboxRight() {
    return this.content.factboxRight;
  }

  get extraData() {
    return this.content.extraData;
  }

  get featuredImage() {
    return this.content.image;
  }

  get extraDataFooter() {
    return this.content.extraDataFooter;
  }

  get footNotes() {
    return this.content.footNotes;
  } // get relatedArticlesIds() {
  //   return this.content.relatedArticlesIds;
  // }


  get ctaText() {
    return '<a href="https://www.gyldendal.no/om-gyldendal/nyhetsbrev/meld-deg-pa-vinduets-nyhetsbrev/" target="_blank">Meld deg på Vinduets nyhetsbrev!</a>';
  }

  get socialShares() {
    const socials = ["twitter", "facebook", "linkedin"];
    const host = process.env.BASE_URL;
    const url = `${host}${this.$route.fullPath}`;
    const headline = this.content.title;
    return socials.map(type => {
      const item = {
        target: "_blank",
        url: "",
        type
      };

      switch (type) {
        case "twitter":
          item.url = `https://twitter.com/intent/tweet?url=${url}&text=${headline}`;
          item.name = "Share on Twitter";
          break;

        case "facebook":
          item.url = `https://www.facebook.com/sharer.php?u=${url}`;
          item.name = "Share on Facebook";
          break;

        case "linkedin":
          item.url = `http://www.linkedin.com/shareArticle?mini=true&url=${url}&title=${headline}`;
          item.name = "Share on Linkedin";
          break;

        default:
          break;
      }

      return item;
    });
  }

};

ArticlePagevue_type_script_lang_ts_decorate([Object(nuxt_property_decorator_common["Prop"])()], ArticlePagevue_type_script_lang_ts_ArticlePageTemplate.prototype, "content", void 0);

ArticlePagevue_type_script_lang_ts_ArticlePageTemplate = ArticlePagevue_type_script_lang_ts_decorate([Object(nuxt_property_decorator_common["Component"])({
  components: {
    SocialLinks: SocialLinks,
    NewsletterCta: common_NewsletterCta,
    RelatedArticles: RelatedArticles
  }
})], ArticlePagevue_type_script_lang_ts_ArticlePageTemplate);
/* harmony default export */ var ArticlePagevue_type_script_lang_ts_ = (ArticlePagevue_type_script_lang_ts_ArticlePageTemplate);
// CONCATENATED MODULE: ./views/ArticlePage.vue?vue&type=script&lang=ts&
 /* harmony default export */ var views_ArticlePagevue_type_script_lang_ts_ = (ArticlePagevue_type_script_lang_ts_); 
// CONCATENATED MODULE: ./views/ArticlePage.vue



function ArticlePage_injectStyles (context) {
  
  
}

/* normalize component */

var ArticlePage_component = Object(componentNormalizer["a" /* default */])(
  views_ArticlePagevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  ArticlePage_injectStyles,
  "f47b33ac",
  "df6281be"
  
)

/* harmony default export */ var ArticlePage = __webpack_exports__["default"] = (ArticlePage_component.exports);

/***/ })

};;
//# sourceMappingURL=4.js.map