# `Foundation for SPA + CMS (Nuxt.js) project`

## Build Setup

```bash
# install dependencies
$ npm run install

# FOR DEVELOPMENT: serve with hot reload at localhost:3000
$ npm run dev

# FOR LAUNCH FAKE API SERVER: go to cd json-server-multiple-files-master
# Install globally json-server
$ npm install -g json-server

# Then run this command
$ json-server --watch ./fakeapis/index.js --port 4000 --routes routes.json


# FOR PRODUCTION: build and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Enviroment variables

Create .env in root and add

```bash
NODE_ENV=development
API_URL=http://localhost:4000
BASE_URL=http://localhost:3200
```
