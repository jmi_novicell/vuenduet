import {
  INavigationItem,
  ISiteHeader,
  ISiteFooter,
  ISiteSettings,
  IBrand,
} from '.';

export interface IRootState {}

export interface INavigationState {
  primary: INavigationItem[];
  secondary: INavigationItem[];
  path: string;
  editorData: any;
  navigationText: any;
  editorialBoardContent: any;
  columnText1Content: any;
  columnText2Content: any;
}

export interface ISiteState {
  brand?: IBrand;
  settings?: ISiteSettings;
  header?: ISiteHeader;
  footer?: ISiteFooter;
  vinduetSiteSettings?: any;
  vinduetSiteFooter?: any;
}
