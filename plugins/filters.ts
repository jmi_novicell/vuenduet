import Vue from "vue";

Vue.filter("norwegianFormatDate", (value: any) => {
  const date = new Date(value);
  return date.toLocaleString(["nb-NO"], {
    month: "2-digit",
    day: "2-digit",
    year: "numeric",
  });
});
