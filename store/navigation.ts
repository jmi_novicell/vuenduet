import { MutationTree, GetterTree, ActionTree } from "vuex";
import {
  INavigationState,
  IRootState,
  INavigation,
  INavigationItem,
} from "~/types";

export const state = (): INavigationState => ({
  primary: [],
  secondary: [],
  path: "",
  editorData: [],
  navigationText: [],
  editorialBoardContent: [],
  columnText1Content: [],
  columnText2Content: [],
});

export const mutations: MutationTree<INavigationState> = {
  setPrimaryItems(state: INavigationState, items: INavigationItem[]): void {
    state.primary = items;
  },
  setSecondaryItems(state: INavigationState, items: INavigationItem[]): void {
    state.secondary = items;
  },
  setPath(state: INavigationState, path: string): void {
    state.path = path;
  },
  setEditorData(state: INavigationState, editorData: any[]): void {
    state.editorData = editorData;
  },
  setNavigaitionText(state: INavigationState, navigationText: any[]): void {
    state.navigationText = navigationText;
  },
  setEditorialBoardContent(state: INavigationState, editorialBoardContent: any[]): void {
    state.editorialBoardContent = editorialBoardContent;
  },
  setColumnText1Content(state: INavigationState, columnText1Content: any[]): void {
    state.columnText1Content = columnText1Content;
  },
  setColumnText2Content(state: INavigationState, columnText2Content: any[]): void {
    state.columnText2Content = columnText2Content;
  },
};

export const getters: GetterTree<INavigationState, IRootState> = {
  getPrimaryItems(state: INavigationState): any[] {
    return state.primary || [];
  },
  getSecondaryItems(state: INavigationState): any[] {
    return state.secondary || [];
  },
  getPath(state: INavigationState): string {
    return state.path;
  },
  getEditorData(state: INavigationState): any[] {
    return state.editorData;
  },
  getNavigationText(state: INavigationState): any[] {
    return state.navigationText;
  },
  getEditorialBoardContent(state: INavigationState): any[] {
    return state.editorialBoardContent;
  },
  getColumnText1Content(state: INavigationState): any[] {
    return state.columnText1Content;
  },
  getColumnText2Content(state: INavigationState): any[] {
    return state.columnText2Content;
  },
};

export const actions: ActionTree<INavigationState, IRootState> = {
  SET_PATH({ commit }: { commit: any }, { path }: { path: string }) {
    commit("setPath", path);
  },

  async FETCH_NAVIGATION({ commit }: { commit: any }, { path }: { path: string }) {
    const { data }: { data: INavigation } = await (this as any).$contentApi.getSettings(path);
    // eslint-disable-next-line no-console
    console.log("valor del data de fetch_navigation navigationItems ------------>>>>> ", data.vinduetSiteNavigation.navigationItems);
    commit("setPrimaryItems", data.vinduetSiteNavigation.navigationItems);
    // commit("setSecondaryItems", data.secondary);
  },

  async FETCH_NAVIGATION_DATA({ commit }: { commit: any }, { path }: { path: string }) {
    const { data }: { data: any } = await (this as any).$contentApi.getSettings(path);
    // eslint-disable-next-line no-console
    console.log("valor del data de fetch_navigation data ------------>>>>> ", data.vinduetSiteNavigation.columnText1);
    // commit("setEditorData", data.editorsContent);
    // commit("setNavigaitionText", data.navigationText);
    // commit("setEditorialBoardContent", data.editorialBoardContent);
    commit("setColumnText1Content", data.vinduetSiteNavigation.columnText1);
    commit("setColumnText2Content", data.vinduetSiteNavigation.columnText2);
  },
};
