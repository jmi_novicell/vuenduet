import { MutationTree, GetterTree, ActionTree } from "vuex";
import {
  IRootState,
  ISiteState,
  IBrand,
  ISiteHeader,
  ISiteFooter,
  ISettings,
  ISiteSettings,
} from "~/types";

export const state = (): ISiteState => ({
  brand: {},
  footer: {},
  header: {},
  settings: {},
  vinduetSiteSettings: {},
  vinduetSiteFooter: {},
});

export const mutations: MutationTree<ISiteState> = {
  setBrand(state: ISiteState, brand: IBrand): void {
    state.brand = brand;
  },
  setHeader(state: ISiteState, header: ISiteHeader): void {
    state.header = header;
  },
  setFooter(state: ISiteState, footer: ISiteFooter): void {
    state.footer = footer;
  },
  setSettings(state: ISiteState, settings: ISiteSettings): void {
    state.settings = settings;
  },
  setVinduetSiteSettings(state: ISiteState, vinduetSiteSettings: ISiteSettings): void {
    state.vinduetSiteSettings = vinduetSiteSettings;
  },
  setVinduetSiteFooter(state: ISiteState, vinduetSiteFooter: ISiteFooter): void {
    state.vinduetSiteFooter = vinduetSiteFooter;
  },
};

export const getters: GetterTree<ISiteState, IRootState> = {
  getFooter(state: ISiteState) {
    return state.footer;
  },
  getHeader(state: ISiteState) {
    return state.header;
  },
  getVinduetSiteSettings(state: ISiteState) {
    return state.vinduetSiteSettings;
  },
  getVinduetSiteFooter(state: ISiteState) {
    return state.vinduetSiteFooter;
  },
  getSegments(state: ISiteState) {
    return state.settings ? state.settings.segments : [];
  },
  getLegalLinks(state: ISiteState) {
    return state.settings ? state.settings.legalLinks : [];
  },
  getContactData(state: ISiteState) {
    return state.settings ? state.settings.contactData : {};
  },
  getEditorData(state: ISiteState) {
    return state.settings ? state.settings.editorData : {};
  },
  getPublisherData(state: ISiteState) {
    return state.settings ? state.settings.publisherData : {};
  },
  getLogoData(state: ISiteState) {
    return state.brand;
  },
};

export const actions: ActionTree<ISiteState, IRootState> = {
  async FETCH_SETTINGS({ commit }: { commit: any }, { path }: { path: string }) {
    try {
      const { data }: { data: ISettings } = await (this as any).$contentApi.getSettings(path);
      // eslint-disable-next-line no-console
      console.log("valor del data vinduetSiteFooter ---------------> ", data.vinduetSiteSettings);
      // eslint-disable-next-line no-console
      console.log("valor del data vinduetSiteFooter ---------------> ", data.vinduetSiteFooter);
      // commit("setFooter", data.siteFooter);
      // commit("setHeader", data.siteHeader);
      // commit("setSettings", data.siteSettings);
      commit("setBrand", data.vinduetSiteSettings.logo);
      commit("setVinduetSiteSettings", data.vinduetSiteSettings);
      commit("setVinduetSiteFooter", data.vinduetSiteFooter);
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error("Error on fetching settings");
    }
  },
};
