import { MutationTree, ActionTree } from 'vuex';
import { IRootState } from '~/types';

export const state = (): IRootState => ({});

export const mutations: MutationTree<IRootState> = {};

export const actions: ActionTree<IRootState, IRootState> = {
  async nuxtServerInit({ dispatch }: { dispatch: any }, ctx) {
    const path = `${ctx.env.baseUrl}/`;
    const APIPath = `${ctx.env.apiUrl}/`;
    // eslint-disable-next-line no-console
    console.log(path);
    // eslint-disable-next-line no-console
    console.log(APIPath);
    const fetchNavigation = dispatch('navigation/FETCH_NAVIGATION', { path }, { root: true });
    const fetchNavigationData = dispatch('navigation/FETCH_NAVIGATION_DATA', { path }, { root: true });
    const fetchSettings = dispatch('site/FETCH_SETTINGS', { path });
    try {
      await Promise.all([fetchNavigation, fetchSettings, fetchNavigationData]);
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error('Error on fetching navigation and settings');
    }
  },
};
